package pl.edu.uj.monopoly.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.model.card.*;
import pl.edu.uj.monopoly.model.exception.CardNotFoundException;
import pl.edu.uj.monopoly.service.interfaces.CardService;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;


@Service
public class CardServiceImpl implements CardService {
    private PlayerService playerService;
    private Board board;

    @Autowired
    public CardServiceImpl(PlayerService playerService,
                           Board board) {

        this.playerService = playerService;
        this.board = board;
    }

    @Override
    public Integer getCardCost(Card card) {
        return card.getPrice();
    }

    @Override
    public int getPenalizationCost(int cardId) {
        Card card = getCardById(cardId);
        Player cardOwner = getOwner(card);

        if (cardOwner == null) {

            return 0;
        }

        switch (card.getType()) {
            case CITY_CARD:

                CityCard penalizationCityCard = (CityCard) card;

                int noHouse = penalizationCityCard.getNumberOfHouses();
                int noHotels = penalizationCityCard.getNumberOfHotels();

                if (noHotels > 0) {

                    return penalizationCityCard.getPenalizationCosts().get((penalizationCityCard.getPenalizationCosts().size()) - 1);

                } else if (noHouse > 0) {

                    return penalizationCityCard.getPenalizationCosts().get(noHouse);

                } else if (hasPlayerAllCardsOfSameColor(cardOwner, penalizationCityCard.getColor())) {

                    return penalizationCityCard.getPenalizationCosts().get(0) * 2;

                } else {

                    return penalizationCityCard.getPenalizationCosts().get(0);
                }


            case UTILITY:

                List<Card> cardTypeOwnsUtility = playerService.getOwnedCardsByType(cardOwner, CardType.UTILITY);

                //It should be multiplies by number of moves, but number of moves is dynamic...
                return card.getPenalizationCosts().get(cardTypeOwnsUtility.size() - 1);

            case RAILROAD:

                List<Card> cardTypeOwnsRailroad = playerService.getOwnedCardsByType(cardOwner, CardType.RAILROAD);

                return card.getPenalizationCosts().get(cardTypeOwnsRailroad.size() - 1);

            default:
                return 0;
        }



    }

    @Override
    public Card getCardById(int cardId) {
        return board.getFields().stream()
                .filter(card -> card.getCardId() == cardId)
                .findFirst()
                .orElseThrow(CardNotFoundException::new);
    }

    @Override
    public boolean buildHouse(Player currentPlayer, int cardId) {
        if (!(getCardById(cardId) instanceof CityCard)) {
            return false;
        }

        CityCard card = (CityCard) getCardById(cardId);
        CardColor color = card.getColor();

        int numberOfHouses = card.getNumberOfHouses();


        if (!hasPlayerAllCardsOfSameColor(currentPlayer, color)) {

            currentPlayer.addMessageToDisplay("Cannot buy a house for the card, " + card.getName() +  ", because you do not own all cards in colour " + color.name() + ".");
            return false;
        }

        if (numberOfHouses == 4) {

            currentPlayer.addMessageToDisplay("Cannot buy a house for the card, " + card.getName() +  ", because you have already own maximum number of houses (4).");
            return false;
        }

        int playerBalanceAfterHousePurchase = currentPlayer.getAccountBalance() - card.getBuildCost();

        if (playerBalanceAfterHousePurchase < 0) {

            currentPlayer.addMessageToDisplay("Cannot buy a house for the card, " + card.getName() +  ", because you do not have enough money on your account.");
            return false;
        }

        if (!hasPlayerEqualNumberOfHousesOnFields(currentPlayer, color, card)) {

            currentPlayer.addMessageToDisplay("Cannot buy a house for the card, " + card.getName() +  ", because you do not own equal numbers of houses on all cards with colour " + color.name() + ".");
            return false;
        }

        currentPlayer.setAccountBalance(playerBalanceAfterHousePurchase);

        card.setNumberOfHouses(++numberOfHouses);
        currentPlayer.addMessageToDisplay("A house on the card, " + card.getName() +  ", has been bought.");

        return true;
    }

    @Override
    public boolean buildHotel(Player currentPlayer, int cardId) {
        if (!(getCardById(cardId) instanceof CityCard)) {
            return false;
        }

        CityCard card = (CityCard) getCardById(cardId);
        CardColor color = card.getColor();

        int numberOfHouses = card.getNumberOfHouses();
        int numberOfHotels = card.getNumberOfHotels();

        if (numberOfHotels == 1) {

            currentPlayer.addMessageToDisplay("Cannot buy a hotel for the card, " + card.getName() +  ", because you have already own maximum number of hotels (1).");
            return false;
        }

        if (!hasPlayerAllCardsOfSameColor(currentPlayer, color)) {

            currentPlayer.addMessageToDisplay("Cannot buy a hotel for the card, " + card.getName() +  ", because you do not own all cards in colour " + color.name() + ".");
            return false;
        }

        if (numberOfHouses < 4) {

            currentPlayer.addMessageToDisplay("Cannot buy a hotel for the card, " + card.getName() +  ", because you do not own maximum houses for the card (4).");
            return false;
        }

        int playerBalanceAfterHousePurchase = currentPlayer.getAccountBalance() - card.getBuildCost();

        if (playerBalanceAfterHousePurchase < 0) {

            currentPlayer.addMessageToDisplay("Cannot buy a hotel for the card, " + card.getName() +  ", because you do not have enough money on your account.");
            return false;
        }

        currentPlayer.setAccountBalance(playerBalanceAfterHousePurchase);

        card.setNumberOfHouses(0);
        card.setNumberOfHotels(1);

        currentPlayer.addMessageToDisplay("A hotel on the card, " + card.getName() +  ", has been bought.");

        return true;
    }

    @Override
    public Boolean isCardOwned(Card card) {

        List<Player> players = playerService.getAllPlayers();
        for (Player player : players) {
            List<Card> cards = playerService.getOwnedCards(player);
            if (cards.contains(card))
                return true;
        }
        return false;
    }

    @Override
    public Player getOwner(Card card) {

        List<Player> players = playerService.getAllPlayers();
        for (Player player : players) {
            List<Card> cards = playerService.getOwnedCards(player);
            for (Card boughtCard: cards) {
                if (boughtCard.getCardId().equals(card.getCardId()))
                    return player;
            }
        }
        return null;
    }

    @Override
    public Boolean isSpecialCard(Card card) {
        return card.isSpecial();
    }

    @Override
    public Board getBoard() {
        return this.board;
    }

    private boolean hasPlayerEqualNumberOfHousesOnFields(Player currentPlayer, CardColor color, CityCard currentCard) {
        List<CityCard> cardOfChosenColor = currentPlayer.getCards().stream()
                .filter(c -> c instanceof CityCard)
                .filter(c -> c.getColor().equals(color))
                .map(CityCard.class::cast)
                .collect(Collectors.toList());

        int numberOfHousesOnChosenCard = currentCard.getNumberOfHouses();

        int cardWithFewestNumberOfHouses = cardOfChosenColor.stream()
                .map(CityCard::getNumberOfHouses)
                .min(Integer::compare)
                .orElseThrow(NoSuchElementException::new);

        ++numberOfHousesOnChosenCard;

        return Math.abs(numberOfHousesOnChosenCard - cardWithFewestNumberOfHouses) <= 1;
    }

    public int getNumberOfOwnedCardsWithSameColor(Player currentPlayer, CardColor color) {
        return currentPlayer.getCards().stream()
                .filter(c -> c.getColor() == color)
                .collect(Collectors.toList()).size();
    }

    public boolean hasPlayerAllCardsOfSameColor(Player currentPlayer, CardColor color) {

        int numberOfCardsWithSameColorOnBoard = (int) board.getFields().stream()
                .filter(field -> field.getColor() == color)
                .count();

        return getNumberOfOwnedCardsWithSameColor(currentPlayer, color) == numberOfCardsWithSameColorOnBoard;
    }
}