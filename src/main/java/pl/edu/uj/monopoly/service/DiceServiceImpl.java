package pl.edu.uj.monopoly.service;

import org.springframework.stereotype.Service;
import pl.edu.uj.monopoly.service.interfaces.DiceService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class DiceServiceImpl implements DiceService {
    private static final Integer MIN = 1;
    private static final Integer MAX = 6;
    private Boolean was_double = false;

    @Override
    public List<Integer> rollDice() {
        was_double = false;
        int firstRoll = rollADice();
        int secondRoll = rollADice();

        if (firstRoll == secondRoll) {
            was_double = true;
        }

        ArrayList<Integer> retList = new ArrayList<>();

        retList.add(firstRoll + secondRoll);
        retList.add(firstRoll);
        retList.add(secondRoll);

        return retList;

    }

    private Integer rollADice() {
        Random rn = new Random();
        return MIN + rn.nextInt(MAX - MIN + 1);
    }

    @Override
    public Boolean wasDouble() {
        return was_double;
    }

    @Override
    public void resetDouble() {
        was_double = false;
    }
}
