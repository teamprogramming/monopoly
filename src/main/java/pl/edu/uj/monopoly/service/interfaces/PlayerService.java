package pl.edu.uj.monopoly.service.interfaces;

import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.model.card.Card;
import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.card.CardType;

import java.util.List;

public interface PlayerService {

    Player getPlayerById(Integer playerId);

    List<Player> getAllPlayers();

    void addNewPlayer(String name);

    List<Card> getOwnedCards(Player player);

    List<Card> getOwnedCardsByType(Player player, CardType cardType);

    Integer getOwnedMoney(Player player);

    void buyCard(Player player);

    boolean isGameStarted();

    int getNumberOfPlayersConnected();

    Integer getMaximumNumberOfPlayingPlayers();

    void checkIfStoppedOnUtilities(Player player, Integer numberOfMoves, Board board);

    void checkIfStoppedOnRailroads(Player player, Board board);

    void checkIfStoppedOnTax(Player player, Board board);

    void checkIfStoppedOnGoToJail(Player player, Board board);

    void checkIfStoppedOnCity(Player player, CardService cardService);

    void checkIfStoppedOnChance(Player player, Board board);

    void checkIfStoppedOnCommunityChest(Player player, Board board);

    void checkIfStoppedOnParking(Player player, Board board);

    void checkIfPlayerBankrupt(Player player);

}
