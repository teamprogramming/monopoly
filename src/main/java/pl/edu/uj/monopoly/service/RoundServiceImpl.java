package pl.edu.uj.monopoly.service;

import org.springframework.stereotype.Service;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;
import pl.edu.uj.monopoly.service.interfaces.RoundService;

@Service
public class RoundServiceImpl implements RoundService {

    final private Integer BOARD_SIZE = 40;
    final private int GRANT_FOR_PASSING_GO = 200;

    private PlayerService playerService;
    private int currentPlayerId;

    public RoundServiceImpl(PlayerService playerService) {
        this.playerService = playerService;
        this.currentPlayerId = 0;
    }

    @Override
    public Player getNextPlayer() {

        long playersWithMoney = playerService.getAllPlayers().stream()
                .filter(p -> !p.isBankrupt())
                .count();

        if (playersWithMoney == 0L) {
            return null;
        }

        Player nextPlayer;
        do {
            int nextPlayerId = (currentPlayerId + 1) % playerService.getMaximumNumberOfPlayingPlayers();
            this.currentPlayerId = nextPlayerId;

            nextPlayer = playerService.getAllPlayers().stream()
                    .filter(p -> p.getPlayerId().equals(nextPlayerId) && !p.isBankrupt())
                    .findAny()
                    .orElse(null);
        } while (nextPlayer == null);

        return nextPlayer;
    }

    @Override
    public Player getCurrentPlayerTurn() {
        return playerService.getAllPlayers().stream()
                .filter(p -> p.getPlayerId().equals(currentPlayerId))
                .findAny()
                .orElseThrow(() -> new IllegalStateException("Current player does not exist."));
    }

    @Override
    public Integer getBoardSize() {

        return BOARD_SIZE;
    }

    @Override
    public void movePlayer(Player currentPlayer, Integer numberOfMoves) {
        currentPlayer.setPlayerPosition((numberOfMoves + currentPlayer.getPlayerPosition()) % getBoardSize());
    }

    @Override
    public void checkIfPassedGoWithMoves(Player currentPlayer, Integer numberOfMoves) {
        Integer nextPosition = numberOfMoves + currentPlayer.getPlayerPosition();
        if (nextPosition.compareTo(getBoardSize()) >= 0) {
            currentPlayer.setAccountBalance(currentPlayer.getAccountBalance() + GRANT_FOR_PASSING_GO);
            currentPlayer.addMessageToDisplay("You have passed the GO, you are earning " + GRANT_FOR_PASSING_GO + "$.");
        }
    }


    @Override
    public void checkIfPassedGoWithPosition(Player currentPlayer, Integer nextPosition) {
        if (currentPlayer.getPlayerPosition().compareTo(nextPosition) > 0) {
            currentPlayer.setAccountBalance(currentPlayer.getAccountBalance() + GRANT_FOR_PASSING_GO);
            currentPlayer.addMessageToDisplay("You have passed the GO, you are earning " + GRANT_FOR_PASSING_GO + "$.");
        }
    }
}