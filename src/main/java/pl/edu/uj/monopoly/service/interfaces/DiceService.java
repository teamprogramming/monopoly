package pl.edu.uj.monopoly.service.interfaces;

import java.util.List;

public interface DiceService {
    List<Integer> rollDice();

    Boolean wasDouble();

    void resetDouble();
}