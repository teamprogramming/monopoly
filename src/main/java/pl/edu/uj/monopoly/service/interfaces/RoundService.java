package pl.edu.uj.monopoly.service.interfaces;

import pl.edu.uj.monopoly.model.Player;

public interface RoundService {

    Integer getBoardSize();

    Player getNextPlayer();

    Player getCurrentPlayerTurn();

    void movePlayer(Player currentPlayer, Integer numberOfMovers);

    void checkIfPassedGoWithMoves(Player currentPlayer, Integer numberOfMoves);

    void checkIfPassedGoWithPosition(Player currentPlayer, Integer nextPosition);
}
