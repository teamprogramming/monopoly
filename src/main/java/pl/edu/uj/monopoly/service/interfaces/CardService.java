package pl.edu.uj.monopoly.service.interfaces;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.model.card.Card;
import pl.edu.uj.monopoly.model.card.CardColor;

public interface CardService {
    Integer getCardCost(Card cityCard);

    int getPenalizationCost(int cardId);

    Card getCardById(int cardId);

    boolean buildHouse(Player currentPlayer, int cardId);

    boolean buildHotel(Player currentPlayer, int cardId);

    Boolean isCardOwned(Card cityCard);

    Boolean isSpecialCard(Card card);

    Board getBoard();

    Player getOwner(Card card);

    boolean hasPlayerAllCardsOfSameColor(Player currentPlayer, CardColor color);

    int getNumberOfOwnedCardsWithSameColor(Player currentPlayer, CardColor color);
}