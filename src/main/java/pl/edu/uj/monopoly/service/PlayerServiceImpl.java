package pl.edu.uj.monopoly.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Chances;
import pl.edu.uj.monopoly.model.CommunityChests;
import pl.edu.uj.monopoly.model.EventCards.EventCard;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.model.card.*;
import pl.edu.uj.monopoly.service.interfaces.CardService;
import pl.edu.uj.monopoly.service.interfaces.DiceService;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static pl.edu.uj.monopoly.model.card.CardType.*;

@Service
public class PlayerServiceImpl implements PlayerService {
    private List<Player> players = new ArrayList<>();
    private Board gameBoard;
    private final int MAX_PLAYERS_NUMBER = 4;
    private int numberOfPlayersConnected = 0;
    private boolean isGameStarted = false;
    private Chances chances;
    private CommunityChests communityChests;
    private SimpMessagingTemplate websocket;

    @Autowired
    public PlayerServiceImpl(Board gameBoard,
                             DiceService diceService,
                             SimpMessagingTemplate websocket ) {
        this.gameBoard = gameBoard;
        this.chances = new Chances(diceService, this);
        this.communityChests = new CommunityChests(this);
        this.websocket = websocket;
    }

    @Override
    public Player getPlayerById(Integer playerId) {
        return players.stream()
                .filter(player -> player.getPlayerId().equals(playerId))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Player doesn't exist"));
    }

    @Override
    public List<Player> getAllPlayers() {
        return players;
    }

    @Override
    public void addNewPlayer(String name) {
        if (numberOfPlayersConnected == MAX_PLAYERS_NUMBER) {
            throw new IllegalStateException("The limit of players has been reached");
        }

        players.add(new Player(numberOfPlayersConnected, name, 0));

        ++numberOfPlayersConnected;

        if (numberOfPlayersConnected == MAX_PLAYERS_NUMBER) {
            isGameStarted = true;
        }
    }

    public boolean isGameStarted() {
        return isGameStarted;
    }

    public int getNumberOfPlayersConnected() {
        return numberOfPlayersConnected;
    }

    @Override
    public List<Card> getOwnedCards(Player player) {
        return player.getCards();
    }

    @Override
    public List<Card> getOwnedCardsByType(Player player, CardType cardType) {

        return this.getOwnedCards(player).stream().filter(card -> card.getType() == cardType).collect(Collectors.toList());

    }

    @Override
    public Integer getOwnedMoney(Player player) {
        return player.getAccountBalance();
    }



    @Override
    public void buyCard(Player player) {
        int playerPosition = player.getPlayerPosition();
        Card card = gameBoard.getCardById(playerPosition);

        List<Card> playerCards = player.getCards();


        if (card.isSpecial()) {
            throw new IllegalStateException("The " + card.getName() + " is a special card not for sale");
        }


        switch (card.getType()) {

            case CITY_CARD:
                CityCard cityBuyCard = (CityCard) card;

                if (!cityBuyCard.getCardState().equals(CardState.AVAILABLE)) {
                    throw new IllegalStateException("The" + card.getName() + " is not available");
                }


                if ((player.getAccountBalance() - cityBuyCard.getPrice()) < 0 ) {

                    player.addMessageToDisplay("Cannot buy the card, " + cityBuyCard.getName() + ", you do not have enough money on your account." );
                    return;
                }

                cityBuyCard.setCardState(CardState.PURCHASED);
                player.setAccountBalance(player.getAccountBalance() - cityBuyCard.getPrice());

                playerCards.add(cityBuyCard);
                player.setCards(playerCards);

                player.addMessageToDisplay("The card, " + cityBuyCard.getName() + ", has been bought for " + cityBuyCard.getPrice()  + "$." );


                break;

            case RAILROAD:
                RailroadCard railBuyCard = (RailroadCard) card;

                if (!railBuyCard.getCardState().equals(CardState.AVAILABLE)) {
                    throw new IllegalStateException("The" + card.getName() + " is not available");
                }

                if ((player.getAccountBalance() - railBuyCard.getPrice()) < 0 ) {

                    player.addMessageToDisplay("Cannot buy the card, " + railBuyCard.getName() + ", you do not have enough money on your account." );
                    return;
                }


                railBuyCard.setCardState(CardState.PURCHASED);
                player.setAccountBalance(player.getAccountBalance() - railBuyCard.getPrice());

                playerCards.add(railBuyCard);
                player.setCards(playerCards);
                player.addMessageToDisplay("The card, " + railBuyCard.getName() + ", has been bought for " + railBuyCard.getPrice()  + " $ ." );


                break;

            case UTILITY:
                UtilityCard utilityBuyCard = (UtilityCard) card;

                if (!utilityBuyCard.getCardState().equals(CardState.AVAILABLE)) {
                    throw new IllegalStateException("The" + card.getName() + " is not available");
                }

                if ((player.getAccountBalance() - utilityBuyCard.getPrice()) < 0 ) {

                    player.addMessageToDisplay("Cannot buy the card," + utilityBuyCard.getName() + ", you do not have enough money on your account." );
                    return;
                }

                utilityBuyCard.setCardState(CardState.PURCHASED);
                player.setAccountBalance(player.getAccountBalance() - utilityBuyCard.getPrice());

                playerCards.add(utilityBuyCard);
                player.setCards(playerCards);

                player.addMessageToDisplay("The card, " + utilityBuyCard.getName() + ", has been bought for " + utilityBuyCard.getPrice()  + "$." );


                break;
            default:
                throw new IllegalStateException("The " + card.getName() + " is a " + card.getName() + " card not for sale" );

        }


    }


    public boolean isCardOwnedByPlayer(Integer cardId, Player player) {
        Card card = gameBoard.getCardById(cardId);
        return player.getCards().stream().anyMatch(playerCard -> playerCard.getCardId().equals(card.getCardId()));

    }

    public boolean isCardOwnedByPlayer(Card card, Player player) {

        return player.getCards().stream().anyMatch(playerCard -> playerCard.getCardId().equals(card.getCardId()));

    }

    @Override
    public void checkIfStoppedOnUtilities(Player player, Integer numberOfMoves, Board board) {

        Integer playerPosition = player.getPlayerPosition();
        ArrayList<Card> cardList = board.getFields();

        Card currentCard = cardList.get(playerPosition);

        assert currentCard.getCardId().equals(playerPosition);

        //Not pay to anyone
        if (currentCard.getType().equals(UTILITY) && !isCardOwnedByPlayer(currentCard, player)) {
            //Someone else owns the card
            List<Player> cardOwners = getAllPlayers().stream().filter(owner -> isCardOwnedByPlayer(currentCard, owner)).collect(Collectors.toList());


            if (cardOwners.isEmpty()) {

                return;
            }

            Player cardOwner = cardOwners.get(0);

            List<Card> cardTypeOwns = getOwnedCardsByType(cardOwner, CardType.UTILITY);
            assert !cardTypeOwns.isEmpty();
            assert cardTypeOwns.size() <= 2;

            //getPenalizationCost returns list of multiplications
            Integer penalization = numberOfMoves * ((UtilityCard) currentCard).getPenalizationCosts().get(cardTypeOwns.size() - 1);

            player.setAccountBalance(player.getAccountBalance() - penalization);
            player.addMessageToDisplay("Stopped on " + currentCard.getName() + ", you are paying " +   penalization.toString() + "$ to player " + cardOwner.getName() + ".");

            cardOwner.setAccountBalance(cardOwner.getAccountBalance() + penalization);
            websocket.convertAndSend("/topic/player/moves", cardOwner);
        }

    }

    @Override
    public void checkIfStoppedOnRailroads(Player player, Board board) {

        Integer playerPosition = player.getPlayerPosition();
        ArrayList<Card> cardList = board.getFields();

        Card currentCard = cardList.get(playerPosition);

        assert currentCard.getCardId().equals(playerPosition);

        if (currentCard.getType().equals(RAILROAD) && !isCardOwnedByPlayer(currentCard, player)) {
            //Someone else owns the card
            List<Player> cardOwners = getAllPlayers().stream().filter(owner -> isCardOwnedByPlayer(currentCard, owner)).collect(Collectors.toList());


            if (cardOwners.isEmpty()) {

                return;
            }


            Player cardOwner = cardOwners.get(0);

            List<Card> cardTypeOwns = getOwnedCardsByType(cardOwner, CardType.RAILROAD);
            assert !cardTypeOwns.isEmpty();
            assert cardTypeOwns.size() <= 4;


            //getPenalizationCost returns list of multiplications
            Integer penalization = currentCard.getPenalizationCosts().get(cardTypeOwns.size() - 1);

            player.setAccountBalance(player.getAccountBalance() - penalization);
            player.addMessageToDisplay("Stopped on " + currentCard.getName() + ", you are paying " +   penalization.toString() + "$ to player " + cardOwner.getName() + ".");

            cardOwner.setAccountBalance(cardOwner.getAccountBalance() + penalization);
            websocket.convertAndSend("/topic/player/moves", cardOwner);
        }

    }

    @Override
    public void checkIfStoppedOnTax(Player player, Board board) {

        Integer playerPosition = player.getPlayerPosition();
        ArrayList<Card> cardList = board.getFields();

        Card currentCard = cardList.get(playerPosition);

        Integer penalization = 100;

        assert currentCard.getCardId().equals(playerPosition) : currentCard;

        if (currentCard.getType().equals(CardType.TAX)) {

            player.setAccountBalance(player.getAccountBalance() - penalization);
            player.addMessageToDisplay("Stopped on  " + currentCard.getName() + ", you are paying " + penalization.toString() + "$.");


        }
    }

    @Override
    public void checkIfStoppedOnGoToJail(Player player, Board board) {

        Integer playerPosition = player.getPlayerPosition();
        ArrayList<Card> cardList = board.getFields();

        Card currentCard = cardList.get(playerPosition);
        if (currentCard.getType().equals(GO_TO_JAIL)) {
            assert (currentCard.getCardId().equals(30));
            player.sendToJail();

            player.addMessageToDisplay("Stopped on " + currentCard.getName() + ", you are going to the jail.");

        }
    }

    @Override
    public void checkIfStoppedOnChance(Player player, Board board) {
        Integer playerPosition = player.getPlayerPosition();
        ArrayList<Card> cardList = board.getFields();

        Card currentCard = cardList.get(playerPosition);

        if (currentCard.getType().equals(CHANCE)) {
            EventCard chanceCard = chances.getNext();
            player.addMessageToDisplay("You stepped on Chance. Take a card from top.");
            chanceCard.apply(player, board);
        }
    }

    @Override
    public void checkIfStoppedOnCommunityChest(Player player, Board board) {
        Integer playerPosition = player.getPlayerPosition();
        ArrayList<Card> cardList = board.getFields();

        Card currentCard = cardList.get(playerPosition);

        if (currentCard.getType().equals(COMMUNITY_CHEST)) {
            EventCard communityChestCard = communityChests.getNext();
            player.addMessageToDisplay("You stepped on Community Chest. Take a card from top.");
            communityChestCard.apply(player, board);
        }
    }


    @Override
    public void checkIfStoppedOnCity(Player player, CardService cardService) {

        Integer playerPosition = player.getPlayerPosition();
        Board board = cardService.getBoard();
        ArrayList<Card> cardList = board.getFields();

        Card currentCard =  cardList.get(playerPosition);

        assert currentCard.getCardId().equals(playerPosition) : currentCard;
        assert currentCard.getType() == CardType.CITY_CARD : currentCard.getType();

        List<Player> cardOwners = getAllPlayers().stream().filter(owner -> isCardOwnedByPlayer(currentCard, owner)).collect(Collectors.toList());


        if (!cardOwners.isEmpty() && currentCard.getType().equals(CITY_CARD) && !isCardOwnedByPlayer(currentCard, player)) {

            Player cardOwner = cardOwners.get(0);

            int noHouse = ((CityCard) currentCard).getNumberOfHouses();
            int noHotels = ((CityCard) currentCard).getNumberOfHotels();
            Integer rent;

            if (noHotels > 0) {

                rent = currentCard.getPenalizationCosts().get((currentCard.getPenalizationCosts().size()) - 1);

            } else if (noHouse > 0) {

                rent = currentCard.getPenalizationCosts().get(noHouse);

            } else if (cardService.hasPlayerAllCardsOfSameColor(cardOwner, currentCard.getColor())) {

                rent =  currentCard.getPenalizationCosts().get(0) * 2;

            } else {

                rent = currentCard.getPenalizationCosts().get(0);
            }

            player.addMessageToDisplay("Stopped on " + currentCard.getName() + ", you are paying " +   rent.toString() + "$ to player " + cardOwner.getName() + ".");
            player.setAccountBalance(player.getAccountBalance() - rent);
            cardOwner.setAccountBalance(cardOwner.getAccountBalance() + rent);
            websocket.convertAndSend("/topic/player/moves", cardOwner);
        }
    }

    @Override
    public void checkIfStoppedOnParking(Player player, Board board) {
        Integer playerPosition = player.getPlayerPosition();
        ArrayList<Card> cardList = board.getFields();

        Card currentCard = cardList.get(playerPosition);

        if (currentCard.getType().equals(PARKING)) {

            player.addMessageToDisplay("You have stepped on parking, just wait.");
        }

    }

    public void checkIfPlayerBankrupt(Player player) {

        if (player.isBankrupt()) {

            List<Card> playerCards = player.getCards();

            for (Card card: playerCards ) {
                card.resetState();
            }

            player.setHasGetOutOfJailCard(false);
            playerCards.clear();
            player.addMessageToDisplay("!!!BANKRUPTCY!!! Oh dear, We are out of money!");
        }
    }

    @Override
    public Integer getMaximumNumberOfPlayingPlayers() {
        return MAX_PLAYERS_NUMBER;
    }
}
