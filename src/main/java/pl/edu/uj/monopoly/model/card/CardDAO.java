package pl.edu.uj.monopoly.model.card;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CardDAO {
    private Map<Integer, Card> cards = new HashMap<>();

    public Card getCardById(Integer cardId) {
        return cards.get(cardId);
    }

    public void insert(Card card) {
        cards.put(card.getCardId(), card);
    }
}