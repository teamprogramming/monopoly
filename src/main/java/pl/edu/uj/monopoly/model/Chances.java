package pl.edu.uj.monopoly.model;

import pl.edu.uj.monopoly.model.EventCards.*;
import pl.edu.uj.monopoly.service.interfaces.CardService;
import pl.edu.uj.monopoly.service.interfaces.DiceService;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Chances {
    private List<EventCard> cards;
    private Integer lastIndex = 0;

    public Chances(DiceService diceService, PlayerService playerService) {
        cards = new ArrayList<>(15);

        cards.add(new AdvanceToGo());
        cards.add(new AdvanceToCity("Illinois Ave", 24, playerService));
        cards.add(new AdvanceToCity("St. Charles Place", 11, playerService));
        cards.add(new AdvanceToNearestUtility(playerService,  diceService));
        cards.add(new AdvanceToNearestRailroad(playerService));
        cards.add(new MoneyChange("Bank pays you", "Bank pays you dividend of $50 ", 50));
        cards.add(new GetOutOfJail());
        cards.add(new GoBack());
        cards.add(new GoToJail());
        cards.add(new MakeRepairs(25, 100, "Make general repairs on all your property", "For each house pay $25 – For each hotel $100", playerService));
        cards.add(new MoneyChange("Pay Tax", "Pay poor tax of $15", -15));
        cards.add(new AdvanceToCity("Reading Railroad", 5, playerService));
        cards.add(new AdvanceToCity("Boardwalk", 39, playerService));
        cards.add(new ChangeMoneyWithEveryPlayer(-50, "You have been elected Chairman of the Board", "Pay each player $50", playerService));
        cards.add(new MoneyChange("Your building loan matures", "Collect $150", 150));

//        cards.add(new );

        Collections.shuffle(cards);
    }

    public EventCard getNext() {
        EventCard out =  cards.get(lastIndex);
        lastIndex = (lastIndex + 1) % cards.size();
        return out;
    }
}
