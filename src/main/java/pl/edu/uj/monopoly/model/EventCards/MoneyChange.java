package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;

public class MoneyChange extends EventCard {
    private Integer amount;

    public MoneyChange(String name, String message, Integer amount) {
       this.name = name;
        this.message = message;
        this.amount = amount;
    }

    @Override
    public void apply(Player player, Board board) {
        super.apply(player, board);

        player.setAccountBalance(player.getAccountBalance() + amount);
    }

}