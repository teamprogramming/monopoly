package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;

import java.util.List;

public class ChangeMoneyWithEveryPlayer extends EventCard {
    private PlayerService playerService;
    private Integer amount;

    public ChangeMoneyWithEveryPlayer(Integer amount, String name, String message, PlayerService playerService) {
        this.amount = amount;
        this.playerService = playerService;
        this.name = name;
        this.message = message;
    }

    @Override
    public void apply(Player player, Board board) {
        super.apply(player, board);

        List<Player> players = playerService.getAllPlayers();
        Integer totalAmount = 0;

        for (Player otherPlayer : players)
        {
            if (player.equals(otherPlayer)){
                continue;
            }

            otherPlayer.setAccountBalance(otherPlayer.getAccountBalance() - amount);
            player.setAccountBalance(player.getAccountBalance() + amount);
            totalAmount += amount;
        }

        if (totalAmount < 0) {
            player.addMessageToDisplay("You've payed $" + (-totalAmount) + " total");
        }
        else {
            player.addMessageToDisplay("You've earned $" + totalAmount + " total");

        }
    }

}