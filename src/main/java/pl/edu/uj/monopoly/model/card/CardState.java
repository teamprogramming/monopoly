package pl.edu.uj.monopoly.model.card;

public enum CardState {
    AVAILABLE,
    PURCHASED,
    PLEDGED
}
