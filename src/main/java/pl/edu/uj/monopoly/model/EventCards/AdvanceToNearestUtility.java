package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.model.card.Card;
import pl.edu.uj.monopoly.model.card.CardType;
import pl.edu.uj.monopoly.model.card.UtilityCard;
import pl.edu.uj.monopoly.service.interfaces.DiceService;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;

import java.util.List;

public class AdvanceToNearestUtility extends EventCard {

    private PlayerService playerService;
    private DiceService diceService;

    public AdvanceToNearestUtility(PlayerService playerService, DiceService diceService) {
        this.playerService = playerService;
        this.diceService = diceService;
        name = "Advance token to nearest Utility";
        message = "If owned, throw dice and pay owner a total ten times the amount thrown.";
    }

    @Override
    public void apply(Player player, Board board) {
        super.apply(player, board);

        UtilityCard utility = getNextUtility(player, board);

        List<Player> players = playerService.getAllPlayers();
        boolean ownedByOtherPlayer = false;
        boolean ownedByCurrentPlayer = false;
        Player owner = null;

        for(Player otherPlayer : players)
        {
            for (Card ut : playerService.getOwnedCardsByType(otherPlayer, CardType.UTILITY)) {

                if (ut.getCardId().equals(utility.getCardId())) {

                    if (otherPlayer.equals(player)) {
                        ownedByCurrentPlayer = true;
                    } else {
                        ownedByOtherPlayer = true;
                        owner = otherPlayer;

                    }
                }
            }
        }

        // check if passed GO
        if (player.getPlayerPosition().compareTo(utility.getCardId()) > 0) {
            player.setAccountBalance(player.getAccountBalance() + 200);
            player.addMessageToDisplay("You've passed GO! Earned $200");
        }
        // move
        player.setPlayerPosition(utility.getCardId());
        player.addMessageToDisplay("You've been moved to " + utility.getName());

        if (ownedByOtherPlayer) {
            Integer amount =  diceService.rollDice().get(0) * 10;

            diceService.resetDouble();


            owner.setAccountBalance(owner.getAccountBalance() + amount);
            player.setAccountBalance(player.getAccountBalance() - amount);


            player.addMessageToDisplay("You've payed $" + amount + " to " + owner.getName() + " for stopping on his utility");
        }
        else if (ownedByCurrentPlayer){
            player.addMessageToDisplay("It's already yours.");
        }
        else {
            player.addMessageToDisplay("It's not occupied. You can buy it.");
        }
    }

    private UtilityCard getNextUtility(Player player, Board board)
    {
        List<Card> fields = board.getFields();
        Integer nearestUtilityPosition = player.getPlayerPosition() + 1;
        while (!fields.get(nearestUtilityPosition).getType().equals(CardType.UTILITY))
        {
            nearestUtilityPosition = (nearestUtilityPosition + 1) % fields.size();
        }
        return (UtilityCard)fields.get(nearestUtilityPosition);
    }

}