package pl.edu.uj.monopoly.model;

public class GameStartState {
    private int playersConnected;
    private boolean isGameStarted;

    public GameStartState(int playersConnected, boolean isGameStarted) {
        this.playersConnected = playersConnected;
        this.isGameStarted = isGameStarted;
    }

    public int getPlayersConnected() {
        return playersConnected;
    }

    public void setPlayersConnected(int playersConnected) {
        this.playersConnected = playersConnected;
    }

    public boolean isGameStarted() {
        return isGameStarted;
    }

    public void setGameStarted(boolean gameStarted) {
        isGameStarted = gameStarted;
    }
}
