package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;

public class GoToJail extends EventCard {
    public  GoToJail() {
        name = "Go to Jail";
        message = "Go directly to Jail.";
    }

    @Override
    public void apply(Player player, Board board) {
        super.apply(player, board);

        player.sendToJail();
    }

}