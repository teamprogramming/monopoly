package pl.edu.uj.monopoly.model.card;

import java.util.List;
import java.util.Objects;

public class CityCard implements Card {
    private final int cardId;
    private final CardColor color;
    private final String name;
    private final int price;
    private final int buildCost;
    private final List<Integer> penalizationCosts;
    private CardState cardState;
    private int numberOfHouses;
    private int numberOfHotels;

    //TODO: Consider creating CityCardBuilder
    public CityCard(int cardId, String name, int price, CardColor color, List<Integer> penalizationCosts, int buildCost) {
        this.cardId = cardId;
        this.name = name;
        this.price = price;
        this.color = color;
        this.penalizationCosts = penalizationCosts;
        this.buildCost = buildCost;
        this.cardState = CardState.AVAILABLE;
        this.numberOfHotels = 0;
        this.numberOfHouses = 0;

        assert(penalizationCosts.size() == 6);
    }

    @Override
    public void resetState() {

        cardState = CardState.AVAILABLE;
        numberOfHotels = 0;
        numberOfHouses = 0;
    }

    @Override
    public Integer getCardId() {
        return cardId;
    }

    @Override
    public Boolean isSpecial() {
        return false;
    }

    @Override
    public int getPrice() {
        return price;
    }

    public Integer getBuildCost() { return buildCost; }

    @Override
    public CardType getType() {
        return CardType.CITY_CARD;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public CardColor getColor() { return color; }

    public List<Integer> getPenalizationCosts() {
        return penalizationCosts;
    }

    public int getNumberOfHouses() {
        return numberOfHouses;
    }

    public void setNumberOfHouses(int numberOfHouses) {
        this.numberOfHouses = numberOfHouses;
    }

    public int getNumberOfHotels() {
        return numberOfHotels;
    }

    public void setNumberOfHotels(int numberOfHotels) {
        this.numberOfHotels = numberOfHotels;
    }

    public CardState getCardState() {
        return cardState;
    }

    public void setCardState(CardState cardState) {
        this.cardState = cardState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityCard cityCard = (CityCard) o;
        return cardId == cityCard.cardId &&
                price == cityCard.price &&
                buildCost == cityCard.buildCost &&
                numberOfHouses == cityCard.numberOfHouses &&
                numberOfHotels == cityCard.numberOfHotels &&
                color == cityCard.color &&
                Objects.equals(name, cityCard.name) &&
                Objects.equals(penalizationCosts, cityCard.penalizationCosts) &&
                cardState == cityCard.cardState;
    }

    @Override
    public int hashCode() {

        return Objects.hash(cardId, color, name, price, buildCost, penalizationCosts, cardState, numberOfHouses, numberOfHotels);
    }
}
