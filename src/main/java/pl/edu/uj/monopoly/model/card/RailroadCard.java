package pl.edu.uj.monopoly.model.card;

import java.util.Arrays;
import java.util.List;

public class RailroadCard implements Card {
    private final Integer cardId;
    private final String name;
    private final Integer price;
    private final List<Integer> penalizationCost;
    private CardState cardState;

    public RailroadCard(Integer cardId, String name, Integer price ) {
        this.cardId = cardId;
        this.name = name;
        this.price = price;
        this.penalizationCost = Arrays.asList(25,50,100,200);
        this.cardState = CardState.AVAILABLE;
    }

    @Override
    public void resetState() {
        cardState = CardState.AVAILABLE;
    }

    @Override
    public Integer getCardId() {
        return cardId;
    }

    @Override
    public Boolean isSpecial() {
        return false;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public CardType getType() {
        return CardType.RAILROAD;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public CardColor getColor() { return  null; }

    public List<Integer> getPenalizationCosts() {
        return penalizationCost;
    }

    public CardState getCardState() {
        return cardState;
    }

    public void setCardState(CardState cardState) {
        this.cardState = cardState;
    }
}
