package pl.edu.uj.monopoly.model.card;

/**
 * If the player lands on a Chance, the player takes a card from the top of the respective pack
 * and performs the instruction given on the card.
 * If the player lands on the Jail space, he or she is "Just Visiting". No penalty applies.
 * If the player lands on the Go to Jail square, he or she must move his token directly to Jail.
 * If the player lands on or passes Go in the course of his or her turn, he or she receives $200 from the Bank.
 * A player has until the beginning of his or her next turn to collect this money.
 * If the player lands on Tax he or she must pay the Bank either $200 or 10% of his or her total assets
 * (cash on hand, property, houses and hotels). In some editions of the game, this is a flat rate of $200.
 */

public enum CardType {
    CITY_CARD,
    CHANCE,
    JAIL,
    GO_TO_JAIL,
    GO,
    TAX,
    UTILITY,
    RAILROAD,
    COMMUNITY_CHEST,
    PARKING
}
