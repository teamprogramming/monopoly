package pl.edu.uj.monopoly.model.card;

import java.util.List;

public interface Card {
    Integer getCardId();

    Boolean isSpecial();

    int getPrice();

    CardType getType();

    String getName();

    CardColor getColor();

    List<Integer> getPenalizationCosts();

    void resetState();

}
