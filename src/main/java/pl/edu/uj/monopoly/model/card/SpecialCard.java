package pl.edu.uj.monopoly.model.card;

import java.util.List;

public class SpecialCard implements Card {
    private final Integer cardId;
    private final CardType type;
    private final String name;

    public SpecialCard(Integer cardId, CardType type, String name ) {
        this.cardId = cardId;
        this.type = type;
        this.name = name;
    }

    @Override
    public void resetState() {
        return;
    }

    @Override
    public Integer getCardId() {
        return cardId;
    }

    @Override
    public Boolean isSpecial() {
        return true;
    }

    @Override
    public int getPrice() {
        return 0;
    }

    @Override
    public CardType getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public CardColor getColor() { return null; }

    @Override
    public List<Integer> getPenalizationCosts() {
        return null;
    }
}