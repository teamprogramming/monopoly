package pl.edu.uj.monopoly.model;

import pl.edu.uj.monopoly.model.EventCards.*;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommunityChests {
    private List<EventCard> cards;
    private Integer lastIndex = 0;

    public CommunityChests( PlayerService playerService) {
        cards = new ArrayList<>(17);

        cards.add(new AdvanceToGo());
        cards.add(new MoneyChange("Bank error in your favor", "Collect $200", 200));
        cards.add(new MoneyChange("Doctor's fees", "Pay $50", -50));
        cards.add(new MoneyChange("Sale stock", "From sale of stock you get $50", 50));
        cards.add(new GetOutOfJail());
        cards.add(new GoToJail());
        cards.add(new ChangeMoneyWithEveryPlayer(50, "Grand Opera Night", " Collect $50 from every player for opening night seats", playerService));
        cards.add(new MoneyChange("Holiday Fund matures", "Receive $100", 100));
        cards.add(new MoneyChange("Income tax refund", "Collect $20", 20));
        cards.add(new MoneyChange("It is your birthday", "Collect $10", 10));
        cards.add(new MoneyChange("Life insurance matures", "Collect $100", 100));
        cards.add(new MoneyChange("Pay hospital fees", "Pay hospital fees of $100", -100));
        cards.add(new MoneyChange("Pay school fees", "Pay school fees of $100", -150));
        cards.add(new MoneyChange("Consultancy", "Receive $25 consultancy fee", 25));
        cards.add(new MakeRepairs(40, 115, "Street repairs", "You are assessed for street repairs – $40 per house", playerService));
        cards.add(new MoneyChange("You have won second prize in a beauty contest", " Collect $10", 10));
        cards.add(new MoneyChange("Inheritage", "You inherit $100", 100));

//        cards.add(new );

        Collections.shuffle(cards);
    }

    public EventCard getNext() {
        EventCard out = cards.get(lastIndex);
        lastIndex = (lastIndex + 1) % cards.size();
        return out;
    }
}
