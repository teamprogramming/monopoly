package pl.edu.uj.monopoly.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CardNotFoundException extends RuntimeException {
    public CardNotFoundException() {
        super("Card not found!");
    }

    public CardNotFoundException(String message) {
        super(message);
    }

    public CardNotFoundException(int cardId) {
        super("Card with id=" + cardId + " not found!");
    }
}
