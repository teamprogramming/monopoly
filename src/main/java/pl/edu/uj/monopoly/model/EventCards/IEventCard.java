package pl.edu.uj.monopoly.model.EventCards;


import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;

public interface IEventCard {

     String getName();

     String getMessage();

     void apply(Player player, Board board);

}
