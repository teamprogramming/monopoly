package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.model.card.Card;
import pl.edu.uj.monopoly.model.card.CardType;
import pl.edu.uj.monopoly.model.card.CityCard;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;

import java.util.List;

public class MakeRepairs extends EventCard {
    private PlayerService playerService;
    private Integer houseFee;
    private Integer hotelFee;

    public  MakeRepairs(Integer house, Integer hotel, String name, String message, PlayerService playerService) {
        this.playerService = playerService;
        this.name = name;
        this.message = message;
        this.houseFee = house;
        this.hotelFee = hotel;
    }

    @Override
    public void apply(Player player, Board board) {
        super.apply(player, board);

        Integer amount = 0;

        List<Card> ownedCities = playerService.getOwnedCardsByType(player, CardType.CITY_CARD);
        for(Card card : ownedCities)
        {
            CityCard city = (CityCard)card;

            amount += city.getNumberOfHouses() * houseFee;
            amount += city.getNumberOfHotels() * hotelFee;
        }

        player.setAccountBalance(player.getAccountBalance() - amount);


        player.addMessageToDisplay("You've payed $" + amount + " for repairs");
    }

}