package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.model.card.Card;
import pl.edu.uj.monopoly.model.card.CardType;


public class AdvanceToGo extends EventCard {
    public AdvanceToGo() {
        name = "Advance to Go";
        message = "Collect $200";
    }

    @Override
    public void apply(Player player, Board board) {
        super.apply(player, board);

        Card goField  = board.getFields().get(0); // lets assume

        assert(goField.getType().equals(CardType.GO));
        assert(goField.getCardId().equals(0));

        player.setPlayerPosition(goField.getCardId());
        player.setAccountBalance(player.getAccountBalance() + 200);
    }

}
