package pl.edu.uj.monopoly.model.card;

public enum CardColor {
    BROWN,
    LIGHT_BLUE,
    PINK,
    ORANGE,
    RED,
    YELLOW,
    GREEN,
    DARK_BLUE
}
