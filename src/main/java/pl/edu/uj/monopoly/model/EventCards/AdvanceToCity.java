package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.model.card.Card;
import pl.edu.uj.monopoly.model.card.CardColor;
import pl.edu.uj.monopoly.model.card.CardType;
import pl.edu.uj.monopoly.model.card.CityCard;
import pl.edu.uj.monopoly.service.interfaces.CardService;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static pl.edu.uj.monopoly.model.card.CardType.CITY_CARD;

public class AdvanceToCity extends EventCard {
    private Integer id;
    private String cityName;
    private PlayerService playerService;

    public AdvanceToCity(String name, Integer id, PlayerService playerService ) {
        this.name = "Advance to " + name;
        this.cityName = name;
        this.id = id;
        message = "If you pass Go, collect $200";
        this.playerService = playerService;
    }

    private int getNumberOfOwnedCardsWithSameColor(Player currentPlayer, CardColor color) {
        return currentPlayer.getCards().stream()
                .filter(c -> c.getColor() == color)
                .collect(Collectors.toList()).size();
    }

    private boolean hasPlayerAllCardsOfSameColor(Player currentPlayer, CardColor color, Board board) {

        int numberOfCardsWithSameColorOnBoard = (int) board.getFields().stream()
                .filter(field -> field.getColor() == color)
                .count();

        return getNumberOfOwnedCardsWithSameColor(currentPlayer, color) == numberOfCardsWithSameColorOnBoard;
    }

    @Override
    public void apply(Player player, Board board) {
        super.apply(player, board);

        Card card  = board.getFields().get(id); // lets assume

        assert(card.getCardId().equals(id));
        assert(card.getName().equals(this.cityName));


        // check if passed go
        if (player.getPlayerPosition().compareTo(card.getCardId())> 0)
        {
            player.setAccountBalance(player.getAccountBalance() + 200);
            player.addMessageToDisplay("You've passed GO! Earned $200");
        }

        // move
        player.setPlayerPosition(card.getCardId());
        player.addMessageToDisplay("You've been moved to " + card.getName());

        // check if occupied
        List<Player> players = playerService.getAllPlayers();
        boolean ownedByOtherPlayer = false;
        boolean ownedByCurrentPlayer = false;
        Player owner = null;

        for (Player otherPlayer : players) {
            for (Card rl : playerService.getOwnedCardsByType(otherPlayer, CardType.CITY_CARD)) {
                if (rl.getCardId().equals(card.getCardId())) {

                    if (otherPlayer.equals(player)) {
                        ownedByCurrentPlayer = true;
                    } else {
                        ownedByOtherPlayer = true;
                        owner = otherPlayer;
                    }
                }
            }
        }

        if (ownedByOtherPlayer) {
            int noHouse = ((CityCard) card).getNumberOfHouses();
            int noHotels = ((CityCard) card).getNumberOfHotels();
            Integer amount;

            if (noHotels > 0) {
                amount = card.getPenalizationCosts().get((card.getPenalizationCosts().size()) - 1);
            } else if (noHouse > 0) {
                amount = card.getPenalizationCosts().get(noHouse);
            } else if (hasPlayerAllCardsOfSameColor(owner, card.getColor(), board)) {
                amount =  card.getPenalizationCosts().get(0) * 2;
            } else {
                amount = card.getPenalizationCosts().get(0);
            }

            owner.setAccountBalance(owner.getAccountBalance() + amount);
            player.setAccountBalance(player.getAccountBalance() - amount);

            player.addMessageToDisplay("You've payed $" + amount + " to " + owner.getName() + " for stopping on his property");

        }
        else if (ownedByCurrentPlayer){
            player.addMessageToDisplay("It's already yours.");
        }
        else {
            player.addMessageToDisplay("It's not occupied. You can buy it.");
        }
    }

}
