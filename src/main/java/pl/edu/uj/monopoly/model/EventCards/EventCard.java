package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;

public abstract class EventCard implements IEventCard {

    protected String message;
    protected String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void apply(Player player, Board board) {
        player.addMessageToDisplay("The card you have drawn is:\n" + name + "\n" + message);
    }
}
