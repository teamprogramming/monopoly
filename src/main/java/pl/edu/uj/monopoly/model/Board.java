package pl.edu.uj.monopoly.model;

import pl.edu.uj.monopoly.model.card.*;

import java.util.ArrayList;
import java.util.Arrays;

public class Board {
    private ArrayList<Card> fields;


    public Board() {
        this.fields = new ArrayList<>();

        this.addCard(new SpecialCard(0, CardType.GO, "GO"));
        this.addCard(new CityCard(1, "Mediterranean Avenue", 60, CardColor.BROWN, Arrays.asList(2, 10, 30, 90, 160, 250), 50));
        this.addCard(new SpecialCard(2, CardType.COMMUNITY_CHEST, "Community chest"));
        this.addCard(new CityCard(3, "Baltic Avenue", 60, CardColor.BROWN, Arrays.asList(4, 20, 60, 180, 320, 450), 50));
        this.addCard(new SpecialCard(4, CardType.TAX, "Income Tax"));
        this.addCard(new RailroadCard(5, "Reading Railroad", 200));
        this.addCard(new CityCard(6, "Oriental Avenue", 100, CardColor.LIGHT_BLUE, Arrays.asList(6, 30, 90, 270, 400, 550), 50));
        this.addCard(new SpecialCard(7, CardType.CHANCE, "Chance"));
        this.addCard(new CityCard(8, "Vermont Avenue", 100, CardColor.LIGHT_BLUE, Arrays.asList(6, 30, 90, 270, 400, 550), 50));
        this.addCard(new CityCard(9, "Connecticut Avenue", 120, CardColor.LIGHT_BLUE, Arrays.asList(8, 40, 100, 300, 450, 600), 50));
        this.addCard(new SpecialCard(10, CardType.JAIL, "Jail"));
        this.addCard(new CityCard(11, "St. Charles Place", 140, CardColor.PINK, Arrays.asList(10, 50, 150, 450, 625), 100));
        this.addCard(new UtilityCard(12, "Electric Company", 150));
        this.addCard(new CityCard(13, "States Avenue", 140, CardColor.PINK, Arrays.asList(10, 50, 150, 450, 625), 100));
        this.addCard(new CityCard(14, "Virginia Avenue", 160, CardColor.PINK, Arrays.asList(12, 60, 180, 500, 700, 900), 100));
        this.addCard(new RailroadCard(15, "Pennsylvania Railroad", 200));
        this.addCard(new CityCard(16, "St. James Place", 180, CardColor.ORANGE, Arrays.asList(14, 70, 200, 550, 750, 950), 100));
        this.addCard(new SpecialCard(17, CardType.COMMUNITY_CHEST, "Community chest"));
        this.addCard(new CityCard(18, "Tennessee Avenue", 180, CardColor.ORANGE, Arrays.asList(14, 70, 200, 550, 750, 950), 100));
        this.addCard(new CityCard(19, "New York Avenue", 200, CardColor.ORANGE, Arrays.asList(16, 80, 220, 600, 800, 1000), 100));
        this.addCard(new SpecialCard(20, CardType.PARKING, "Parking"));
        this.addCard(new CityCard(21, "Kentucky Avenue", 220, CardColor.RED, Arrays.asList(18, 90, 250, 700, 875, 1050), 150));
        this.addCard(new SpecialCard(22, CardType.CHANCE, "Chance"));
        this.addCard(new CityCard(23, "Indiana Avenue", 220, CardColor.RED, Arrays.asList(18, 90, 250, 700, 875, 1050), 150));
        this.addCard(new CityCard(24, "Illinois Avenue", 240, CardColor.RED, Arrays.asList(20, 100, 300, 750, 925, 1100), 150));
        this.addCard(new RailroadCard(25, "B.&O. Railroad", 200));
        this.addCard(new CityCard(26, "Atlantic Avenue", 260, CardColor.YELLOW, Arrays.asList(22, 110, 330, 800, 975, 1150), 150));
        this.addCard(new CityCard(27, "Ventnor Avenue", 260, CardColor.YELLOW, Arrays.asList(22, 110, 330, 800, 975, 1150), 150));
        this.addCard(new UtilityCard(28, "Water Works", 150));
        this.addCard(new CityCard(29, "Marvin Gardens", 280, CardColor.YELLOW, Arrays.asList(24, 120, 360, 850, 1025, 1200), 150));
        this.addCard(new SpecialCard(30, CardType.GO_TO_JAIL, "Go to jail"));
        this.addCard(new CityCard(31, "Pacific Avenue", 300, CardColor.GREEN, Arrays.asList(26, 130, 390, 900, 1100), 200));
        this.addCard(new CityCard(32, "North Carolina Avenue", 300, CardColor.GREEN, Arrays.asList(26, 130, 390, 900, 1100), 200));
        this.addCard(new SpecialCard(33, CardType.COMMUNITY_CHEST, "Community chest"));
        this.addCard(new CityCard(34, "Pennsylvania Avenue", 320, CardColor.GREEN, Arrays.asList(28, 150, 450, 1000, 1200, 1400), 200));
        this.addCard(new RailroadCard(35, "Short Line", 200));
        this.addCard(new SpecialCard(36, CardType.CHANCE, "Chance"));
        this.addCard(new CityCard(37, "Park Place", 350, CardColor.DARK_BLUE, Arrays.asList(35, 175, 500, 1100, 1300, 1500), 200));
        this.addCard(new SpecialCard(38, CardType.TAX, "Luxury Tax"));
        this.addCard(new CityCard(39, "Boardwalk ", 400, CardColor.DARK_BLUE, Arrays.asList(50, 200, 600, 1400, 1700, 2000), 200));

//       this.addCard(new SpecialCard(, "",  ""));
//       this.addCard(new CityCard(, "", , , Arrays.asList()));
    }

    public ArrayList<Card> getFields() {
        return fields;
    }

    public Card getCardById(int cardId) {
        return fields.stream().filter(card -> card.getCardId().equals(cardId))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Card does not exists"));
    }

    public void setFields(ArrayList<Card> fields) {
        this.fields = fields;
    }

    public void setCard(int id, Card card) {
        this.fields.set(id, card);
    }

    public void addCard(Card card) {
        this.fields.add(card);
    }

}
