package pl.edu.uj.monopoly.model;

import pl.edu.uj.monopoly.model.card.Card;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private final Integer playerId;
    private final String name;
    private Integer positionOnBoard;
    private Integer accountBalance;
    private Boolean isBankrupt;
    private Boolean isInJail = false;
    private List<Card> cards = new ArrayList<>();
    private Integer doublesInRow = 0;
    private Integer turnsInJail = 0;
    private List<String> messageToDisplay = new ArrayList<>();
    private Boolean hasGetOutOfJailCard = false;


    public Player(Integer playerId, String name, Integer positionOnBoard) {
        this.playerId = playerId;
        this.name = name;
        this.positionOnBoard = positionOnBoard;
        this.accountBalance = 1500;
        this.isBankrupt = false;
    }

    public Integer getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Integer accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Boolean isBankrupt() {
        isBankrupt = accountBalance.doubleValue() < 0;
        return isBankrupt;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerPosition(Integer newPositionOnBoard) {
        positionOnBoard = newPositionOnBoard;
    }

    public Integer getPlayerPosition() {
        return positionOnBoard;
    }

    public void incrementDoublesCount() {
        doublesInRow++;
    }

    public void resetDoublesCount() {
        doublesInRow = 0;
    }

    public Integer getDoublesInRow() {
        return doublesInRow;
    }

    public void sendToJail() {
        positionOnBoard = 10; // Jail
        isInJail = true;
        doublesInRow = 0;
    }

    public Boolean isInJail() {
        return isInJail;
    }

    public void takeOutFromJail() {
        isInJail = false;
        turnsInJail = 0;
    }

    public void resetMessageToDisplay() {
        messageToDisplay = new ArrayList<>();
    }

    public List<String> getMessageToDisplay() {
        return messageToDisplay;
    }

    public void addMessageToDisplay(String messageToDisplay) {
        this.messageToDisplay.add(messageToDisplay);
    }

    public void addGetOutOfJailCard() {
        hasGetOutOfJailCard = true;
    }

    public void takeGetOutOfJailCard() {
        hasGetOutOfJailCard = false;
    }

    public String getName() {
        return name;
    }

    public Integer getTurnsInJail() {
        return turnsInJail;
    }

    public void setTurnsInJail(Integer turnsInJail) {
        this.turnsInJail = turnsInJail;
    }

    public void increaseTurnsInJail() {
        this.turnsInJail += 1;
    }

    public Boolean getHasGetOutOfJailCard() {
        return hasGetOutOfJailCard;
    }

    public void setHasGetOutOfJailCard(Boolean hasGetOutOfJailCard) {
        this.hasGetOutOfJailCard = hasGetOutOfJailCard;
    }
}
