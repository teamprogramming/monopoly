package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;

public class GetOutOfJail  extends EventCard {
    public  GetOutOfJail() {
        name = "Get out of Jail";
        message = "This card may be kept until needed";
    }

    @Override
    public void apply(Player player, Board board) {
        super.apply(player, board);

        player.addGetOutOfJailCard();
    }

}