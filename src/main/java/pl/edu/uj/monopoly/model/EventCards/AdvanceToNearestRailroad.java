package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.model.card.Card;
import pl.edu.uj.monopoly.model.card.CardType;
import pl.edu.uj.monopoly.model.card.RailroadCard;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;

import java.util.List;

public class AdvanceToNearestRailroad extends EventCard {

    private PlayerService playerService;

    public AdvanceToNearestRailroad(PlayerService playerService) {
        this.playerService = playerService;
        name = "Advance token to the nearest Railroad";
        message = "If owned, pay owner twice the rental to which he is otherwise entitled.";
    }

    @Override
    public void apply(Player player, Board board) {
        super.apply(player, board);

        RailroadCard railroad = getNextRailroad(player, board);

        List<Player> players = playerService.getAllPlayers();
        boolean ownedByOtherPlayer = false;
        boolean ownedByCurrentPlayer = false;
        Player owner = null;

        for (Player otherPlayer : players) {
            for (Card rl : playerService.getOwnedCardsByType(otherPlayer, CardType.RAILROAD)) {

                if (rl.getCardId().equals(railroad.getCardId())) {

                    if (otherPlayer.equals(player)) {
                        ownedByCurrentPlayer = true;
                    } else {
                        ownedByOtherPlayer = true;
                        owner = otherPlayer;

                    }
                }
            }
        }

        // check if passed GO
        if (player.getPlayerPosition().compareTo(railroad.getCardId()) > 0) {
            player.setAccountBalance(player.getAccountBalance() + 200);
            player.addMessageToDisplay("You've passed GO! Earned $200");
        }
        // move
        player.setPlayerPosition(railroad.getCardId());
        player.addMessageToDisplay("You've been moved to " + railroad.getName());

        if (ownedByOtherPlayer) {

            Integer nbOfRailroadsOwned = playerService.getOwnedCardsByType(owner, CardType.RAILROAD).size();
            if (nbOfRailroadsOwned >= 1)
            {
                Integer amount = railroad.getPenalizationCosts().get(nbOfRailroadsOwned -1 ) * 2;

                owner.setAccountBalance(owner.getAccountBalance() + amount);
                player.setAccountBalance(player.getAccountBalance() - amount);

                player.addMessageToDisplay("You've payed $" + amount + " to " + owner.getName() + " for stopping on his railroads");
            }
        }
        else if (ownedByCurrentPlayer){
            player.addMessageToDisplay("It's already yours.");
        }
        else {
            player.addMessageToDisplay("It's not occupied. You can buy it.");
        }
    }

    private RailroadCard getNextRailroad(Player player, Board board) {
        List<Card> fields = board.getFields();
        Integer nearestRailroadPosition = player.getPlayerPosition() + 1;
        while (!fields.get(nearestRailroadPosition).getType().equals(CardType.RAILROAD)) {
            nearestRailroadPosition = (nearestRailroadPosition + 1) % fields.size();
        }
        return (RailroadCard) fields.get(nearestRailroadPosition);
    }

}