package pl.edu.uj.monopoly.model.EventCards;

import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.Player;

public class GoBack  extends EventCard {
    public  GoBack() {
        name = "Go back";
        message = "Go back 3 steps.";
    }

    @Override
    public void apply(Player player, Board board) {
        super.apply(player, board);
        player.setPlayerPosition(player.getPlayerPosition()-3);

        player.addMessageToDisplay("You've been moved to " + board.getCardById(player.getPlayerPosition()).getName());
    }

}