package pl.edu.uj.monopoly.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import pl.edu.uj.monopoly.model.GameStartState;
import pl.edu.uj.monopoly.model.Player;
import pl.edu.uj.monopoly.service.interfaces.CardService;
import pl.edu.uj.monopoly.service.interfaces.DiceService;
import pl.edu.uj.monopoly.service.interfaces.PlayerService;
import pl.edu.uj.monopoly.service.interfaces.RoundService;

import java.util.List;

@RestController
public class PlayerController {
    private PlayerService playerService;
    private RoundService roundService;
    private DiceService diceService;
    private CardService cardService;
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public PlayerController(PlayerService playerService,
                            RoundService roundService,
                            DiceService diceService,
                            CardService cardService,
                            SimpMessagingTemplate simpMessagingTemplate) {
        this.playerService = playerService;
        this.roundService = roundService;
        this.diceService = diceService;
        this.cardService = cardService;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @PostMapping("/player/add/{playerName}")
    public ResponseEntity<String> addPlayer(@PathVariable(name = "playerName") String playerName) {
        playerService.addNewPlayer(playerName);
        List<Player> players = playerService.getAllPlayers();

        simpMessagingTemplate.convertAndSend("/topic/players", players);

        GameStartState gameStartState =
                new GameStartState(playerService.getNumberOfPlayersConnected(), playerService.isGameStarted());
        simpMessagingTemplate.convertAndSend("/topic/game-start-state", gameStartState);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @MessageMapping("/player/buy/{playerId}")
    @SendTo("/topic/bought-properties")
    public Player buyProperty(@DestinationVariable("playerId") Integer playerId) {
        Player currentPlayer = playerService.getPlayerById(playerId);
        playerService.buyCard(currentPlayer);

        return currentPlayer;
    }

    @PostMapping("/build/house/{cardId}")
    public boolean buildHouse(@PathVariable("cardId") int cardId) {
        Player currentPlayer = roundService.getCurrentPlayerTurn();
        boolean isHouseBought = cardService.buildHouse(currentPlayer, cardId);
        simpMessagingTemplate.convertAndSend("/topic/player/moves", currentPlayer);

        return isHouseBought;
    }

    @PostMapping("/build/hotel/{cardId}")
    public boolean buildHotel(@PathVariable("cardId") int cardId) {
        Player currentPlayer = roundService.getCurrentPlayerTurn();
        boolean isHotelBought = cardService.buildHotel(currentPlayer, cardId);
        simpMessagingTemplate.convertAndSend("/topic/player/moves", currentPlayer);

        return isHotelBought;
    }

    @MessageMapping("/player/end-turn")
    @SendTo("/topic/player/next")
    public Player endTurn() {
        return roundService.getNextPlayer();
    }

    @MessageMapping("/player/jail/card/{playerId}")
    @SendTo("/topic/player/moves")
    public Player getOutOfJailWithCard(@DestinationVariable("playerId") int playerId) {
        Player currentPlayer = playerService.getPlayerById(playerId);
        currentPlayer.takeGetOutOfJailCard();
        currentPlayer.takeOutFromJail();

        currentPlayer.addMessageToDisplay("You did use the card Get out of Jail. You are free, enjoy it");

        return currentPlayer;
    }

    @MessageMapping("/player/jail/bail/{playerId}")
    @SendTo("/topic/player/moves")
    public Player getOutOfJailWithMoney(@DestinationVariable("playerId") int playerId) {
        Player currentPlayer = playerService.getPlayerById(playerId);

        final int getOutOfJailCost = 50;
        int accountBalanceAfterJail = currentPlayer.getAccountBalance() - getOutOfJailCost;

        if (accountBalanceAfterJail < 0) {

            currentPlayer.addMessageToDisplay("You do not have enough money on your account for leaving the jail.");
            return currentPlayer;
        }

        currentPlayer.setAccountBalance(accountBalanceAfterJail);
        currentPlayer.takeOutFromJail();

        return currentPlayer;
    }

    @GetMapping("/player/{playerId}/{cardId}")
    public void playerSetPosition(@PathVariable("playerId") int playerId,
                                    @PathVariable("cardId") int cardId) {
        Player player = playerService.getPlayerById(playerId);
        player.setPlayerPosition(cardId);
        simpMessagingTemplate.convertAndSend("/topic/player/moves", player);
    }

    @MessageMapping("/player/roll")
    @SendTo("/topic/player/moves")
    public Player rollAndMovePlayer() {
        Player currentPlayer = roundService.getCurrentPlayerTurn();
        currentPlayer.resetMessageToDisplay();

        return rollAndCheckConditions(currentPlayer);
    }


    private Player rollAndCheckConditions(Player currentPlayer) {
        handleTooLongInJail(currentPlayer);

        List<Integer> dicesStates = diceService.rollDice();
        Integer numberOfMoves = dicesStates.get(0);

        currentPlayer.addMessageToDisplay("You rolled " +  dicesStates.get(1) + " on the first dice and " + dicesStates.get(2)  + " on the second dice.");



        if (diceService.wasDouble()) {
            if (currentPlayer.isInJail())
            {
                currentPlayer.resetDoublesCount();
                currentPlayer.takeOutFromJail();
                currentPlayer.addMessageToDisplay("Hurray, the Double Roll. You are leaving the Alcatraz.");

            }
            else {

                currentPlayer.incrementDoublesCount();
                if (currentPlayer.getDoublesInRow() >= 3) {
                    currentPlayer.sendToJail();
                    currentPlayer.addMessageToDisplay("The police department is going to put you to the jail, they know, that you use the fake dices. Too many Double Rolls in row.");

                    return currentPlayer;
                }
            }
        }
        else{
            currentPlayer.resetDoublesCount();
        }

        if (currentPlayer.isInJail()) {
            return currentPlayer;
        }

        roundService.checkIfPassedGoWithMoves(currentPlayer, numberOfMoves);
        roundService.movePlayer(currentPlayer, numberOfMoves);


        playerService.checkIfStoppedOnCity(currentPlayer, cardService);
        playerService.checkIfStoppedOnUtilities(currentPlayer, numberOfMoves, cardService.getBoard());
        playerService.checkIfStoppedOnRailroads(currentPlayer, cardService.getBoard());
        playerService.checkIfStoppedOnTax(currentPlayer, cardService.getBoard());
        playerService.checkIfStoppedOnGoToJail(currentPlayer, cardService.getBoard());
        playerService.checkIfStoppedOnChance(currentPlayer, cardService.getBoard());
        playerService.checkIfStoppedOnCommunityChest(currentPlayer, cardService.getBoard());
        playerService.checkIfStoppedOnParking(currentPlayer, cardService.getBoard());

        playerService.checkIfPlayerBankrupt(currentPlayer);

        return currentPlayer;
    }

    private void handleTooLongInJail(Player currentPlayer) {
        if (currentPlayer.isInJail())
        {
            currentPlayer.increaseTurnsInJail();
            if (currentPlayer.getTurnsInJail() > 3)
            {
                currentPlayer.takeOutFromJail();
                currentPlayer.setTurnsInJail(0);
                currentPlayer.addMessageToDisplay("You've been in jail for more than 3 turns. Leave and pay $50");
                currentPlayer.setAccountBalance(currentPlayer.getAccountBalance() - 50);
            } else {

                currentPlayer.addMessageToDisplay("You are still at the jail.");

            }
        }
    }

}
