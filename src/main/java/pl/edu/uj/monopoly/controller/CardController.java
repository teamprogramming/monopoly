package pl.edu.uj.monopoly.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.uj.monopoly.model.Board;
import pl.edu.uj.monopoly.model.card.Card;
import pl.edu.uj.monopoly.service.interfaces.CardService;

@RestController
@RequestMapping("/card")
public class CardController {
    private CardService cardService;

    @Autowired
    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @GetMapping("/cost/{card}")
    public int getCardCost(@PathVariable("card") Card card) {
        return cardService.getCardCost(card);
    }

    @GetMapping("/penalization/{cardId}")
    public int getPenalization(@PathVariable("cardId") int cardId) {
        return cardService.getPenalizationCost(cardId);
    }

    @GetMapping("/is-special/{cardId}")
    public boolean isCardSpecial(@PathVariable("cardId") int cardId) {
        Card card = cardService.getCardById(cardId);
        return cardService.isSpecialCard(card);
    }

    @GetMapping("/is-owned/{cardId}")
    public boolean isCardOwned(@PathVariable("cardId") int cardId) {
        Card card = cardService.getCardById(cardId);
        return cardService.isCardOwned(card);
    }

    @GetMapping("/board")
    public Board getBoard() {
        return cardService.getBoard();
    }
}
