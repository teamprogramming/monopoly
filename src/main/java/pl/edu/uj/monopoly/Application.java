package pl.edu.uj.monopoly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import pl.edu.uj.monopoly.model.Board;

@SpringBootApplication
public class Application {

    @Bean
    @Scope("singleton")
    public Board board() {
        return new Board();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
