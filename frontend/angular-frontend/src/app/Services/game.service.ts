import { Injectable } from '@angular/core';
import * as Classes from '.././Classes/Classes'

import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class GameService
	{
	stateManager:Classes.StateManager;
	constructor(private http:HttpClient)
		{
		this.stateManager = new Classes.StateManager(http);
		}

	}
