import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }    from '@angular/forms';

import { AppComponent } from './app.component';

import { AwaitComponent } from './Components/Await/await.component'
import { GameDialogComponent } from './Components/GameDialog/game-dialog.component'
import { FieldComponent } from './Components/Field/field.component'
import { GameComponent } from './Components/Game/game.component'
import { MainComponent } from './Components/Main/main.component'
import { PlayComponent } from './Components/Play/play.component'
import { PlayersComponent } from './Components/Players/players.component'
import { StartComponent } from './Components/Start/start.component'
import { GameTableComponent } from './Components/GameTable/game-table.component'
import { WebsocketDebugComponent } from './Components/WebsocketDebug/websocket-debug.component'
import { PlayerNameComponent } from './Components/PlayerName/player-name.component'
import { FieldDetailComponent } from './Components/FieldDetail/field-detail.component'
import { DollarComponent } from './Components/Dollar/dollar.component'
import { OverComponent } from './Components/Over/over.component'
import { MessagesComponent } from './Components/Messages/messages.component'

import { StartService } from './Services/start.service'
import { GameService } from './Services/game.service'

import { RouterObjModule } from './router-obj.module'
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations:
		[
    AppComponent,
		AwaitComponent,
		GameDialogComponent,
		FieldComponent,
		GameComponent,
		MainComponent,
		PlayComponent,
		PlayersComponent,
		StartComponent,
		GameTableComponent,
		WebsocketDebugComponent,
		PlayerNameComponent,
		FieldDetailComponent,
		DollarComponent,
		OverComponent,
		MessagesComponent
  	],
  imports:
		[
    BrowserModule,RouterObjModule,FormsModule,HttpClientModule
  	],
  providers:
		[
		GameService,StartService
		],
  bootstrap: [AppComponent]
})
export class AppModule { }
