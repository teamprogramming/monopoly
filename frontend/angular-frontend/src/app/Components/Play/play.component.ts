import { Component, Input } from '@angular/core';
import {Game} from '../../Classes/Game';

@Component({
  selector: 'play',
  templateUrl: './play.component.html'
})
export class PlayComponent
{
@Input() game:Game;

}
