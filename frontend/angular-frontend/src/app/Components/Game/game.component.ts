import { Component } from '@angular/core';

import {GameService} from '../../Services/game.service'

@Component({
  selector: 'game',
  templateUrl: './game.component.html'
})
export class GameComponent
{
	constructor(private gameService:GameService)
	{
	this.gameService = gameService;
	//console.log(gameService);
	}

}
