import { Component, Input ,OnChanges } from '@angular/core';
import {Field} from '../../Classes/Field'
import {Game} from '../../Classes/Game'
@Component({
  selector: 'field-detail',
  templateUrl: './field-detail.component.html'
})
export class FieldDetailComponent
{
@Input() field:Field;
canBuildHouse:boolean=false;
canBuildHotel:boolean=false;
ngOnChanges():void
	{
	this.canBuildHouse=(this.checkCanBuild(this.field,true) && this.field.game.isUserCurrent);
	this.canBuildHotel=(this.checkCanBuild(this.field,false) && this.field.game.isUserCurrent);
	}


private checkCanBuild(currentField:any,house:boolean):boolean
	{
	let fieldsOfTheSameColor = this.field.game.fields.filter((f)=>{return f.color == currentField.color})
	let fieldsFilteredByOwner = fieldsOfTheSameColor.filter((f)=>{return (f.owner && currentField.owner && f.owner.id == currentField.owner.id)})
	if (fieldsFilteredByOwner.length != fieldsOfTheSameColor.length)
		{
		return false
		}
	else if(house)
		{
		return (fieldsFilteredByOwner.every((f)=>{return (f.houseNum >= currentField.houseNum)}) && currentField.houseNum < 4)
		}
	else if(!house)
		{
		return (fieldsFilteredByOwner.every((f)=>{return ( f.houseNum == 4 && f.hotelNum >= currentField.hotelNum)}) && currentField.hotelNum < 4)
		}
	}

restBuild(house:boolean,fieldId:number):void
	{
	let building = "house";
	if(!house){building="hotel"}
	var obs =  this.field.game.http.post("/build/"+building+"/"+fieldId,{fieldId:fieldId});
	obs.subscribe(
		data=>{console.log("POST data",data)},
		err=>{console.log("POST err",err)},
		()=>{console.log("POST complete")}
		)
	}
}
