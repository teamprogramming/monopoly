import { Component, Input, OnInit } from '@angular/core';
import { GameService } from '../../Services/game.service'
import {StateManager} from '../../Classes/StateManager'

@Component({
  selector: 'over',
  templateUrl: './over.component.html'
})
export class OverComponent implements OnInit
	{
	constructor(private gameService:GameService)
		{
		this.stateManager = gameService.stateManager;
		}
	//@Input() targetPlayers:number;
	//@Input() waitingPlayers:number;
	//placeholders
	stateManager:StateManager;
	topPlayer:any=null;
	ngOnInit():void
		{
			this.topPlayer = this.stateManager.game.players.filter((x)=>{return(!x.isBankrupt)})[0]

		}
	}
