import { Component, Input ,OnChanges } from '@angular/core';
import { GameService } from '../../Services/game.service'
import {StateManager} from '../../Classes/StateManager'

@Component({
  selector: 'await',
  templateUrl: './await.component.html'
})
export class AwaitComponent
	{
	constructor(private gameService:GameService)
		{
		this.stateManager = gameService.stateManager;
		}
	//@Input() targetPlayers:number;
	//@Input() waitingPlayers:number;
	//placeholders
	stateManager:StateManager;
	name:string=null;
	isReady:boolean=false;
	nameSet:string="";
	isNameRepeating:boolean=false;
	setName()
		{
		this.isReady = true;
		this.nameSet = this.name;
		var self = this;
		this.stateManager.addPlayer(self.name);
		}

	ngOnChanges(simpleChanges:any):void
		{
		var self = this;
		this.isNameRepeating = !(self.stateManager.game.players.every((x)=>{return(x.name!=this.name)}))
		//console.log("changes");
		}
	}
