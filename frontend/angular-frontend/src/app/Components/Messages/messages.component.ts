import { Component, Input, OnInit } from '@angular/core';
import {Game} from '../../Classes/Game';
@Component({
  selector: 'messages',
  templateUrl: './messages.component.html'
})
export class MessagesComponent implements OnInit
{
@Input() game:Game;
messages:string[]=[];
ngOnInit()
	{
	var self = this;
	this.game.initMessageCallback(self.onMsg);
	}
onMsg=(msgs)=>
	{
	//console.log("##################3 > ",msgs,this)
	this.messages = msgs
	}
}
