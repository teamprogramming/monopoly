import { Component, Input } from '@angular/core';
import {Dialog} from '../../Classes/Dialog';
import { GameService } from '../../Services/game.service'

@Component({
  selector: 'game-dialog',
  templateUrl: './game-dialog.component.html'
})
export class GameDialogComponent
{
constructor(private gameService:GameService)
{}
@Input() dialog:Dialog;
lastMoney:number;
rollDice():void
	{
	this.lastMoney = this.gameService.stateManager.game.userPlayer.cash;
	this.dialog.rollDice()
	}
}
