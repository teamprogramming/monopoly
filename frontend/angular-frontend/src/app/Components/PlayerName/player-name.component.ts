import { Component, Input } from '@angular/core';
import {Player} from '../../Classes/Player'
import {Game} from '../../Classes/Game'

@Component({
  selector: 'player-name',
  templateUrl: './player-name.component.html'
})
export class PlayerNameComponent
{
@Input() player:Player;

onMouseover():void
	{
	this.player.game.displayUtility.highlightedPlayer = this.player;
	}
onMouseout():void
	{
	this.player.game.displayUtility.highlightedPlayer = null;
	}
}
