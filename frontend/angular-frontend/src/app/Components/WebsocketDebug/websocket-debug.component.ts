import { Component } from '@angular/core';
import { GameService } from '../../Services/game.service'


@Component({
  selector: 'websocket-debug',
  templateUrl: './websocket-debug.component.html'
})
export class WebsocketDebugComponent
	{
	isHidden:boolean=true;

	//serverURL:String="http://localhost:8080/game";
	serverURL:string="/game";
	subscriptionAddr:string="/topic/player/moves";
	sendAddr:string="/app/player/roll";
	sendText:string="{}";

	playerName:string="joe";
	constructor(private gameService:GameService)
		{
		this.gameService = gameService;
		//this.connect()
		}
	toggleHide():void
		{
		this.isHidden = !this.isHidden;
		console.log(this.isHidden);
		}
	connect():void
		{
		var self = this;
		console.log("trying to connect to: "+self.serverURL);

		this.gameService.stateManager.websocketConnector.connect(self.serverURL,()=>{})
		}
	send():void
		{
		var self = this;
		this.gameService.stateManager.websocketConnector.send(self.sendAddr,JSON.parse(self.sendText))
		}
	subscribe():void
		{
		var self = this;
		this.gameService.stateManager.websocketConnector.subscribe(self.subscriptionAddr,(m:string)=>{})
		}
	disconnect():void
		{
		this.gameService.stateManager.websocketConnector.disconnect();
		}
	//..


	buyHouse():void
		{
		let curp = this.gameService.stateManager.game.currentPlayer.field.position;
		this.gameService.stateManager.game.dialog.restBuild(true,curp);
		}

	buyHotel():void
		{
		let curp = this.gameService.stateManager.game.currentPlayer.field.position;
		this.gameService.stateManager.game.dialog.restBuild(false,curp);
		}

	addPlayer():void
		{
		var self = this;
		this.gameService.stateManager.addPlayer(self.playerName);
		}

	performRoutine():void
		{

		//this.connect();
		var times = 400;
		/*
		const subAddrs = ["/topic/player/moves","/topic/bought-properties","/topic/players","/topic/game-start-state"]
		for(let o of subAddrs)
			{
			times += 200;
			var self = this;
			setTimeout(()=>
				{
				this.gameService.stateManager.websocketConnector.subscribe(o,(m:string)=>{})
				},times)


			}
		*/
		setTimeout(()=>
			{
			for(let o of ["alef","bet","gimmel","bongo"])
				{
				times += 200;
				var self = this;
				setTimeout(()=>
					{
					this.gameService.stateManager.addPlayer(o);
					},times)
				//this.gameService.stateManager.addPlayer(o);
				}
			},200)
		}

	simulateRoll():void
		{
		var self = this;
		this.gameService.stateManager.websocketConnector.send("/app/player/roll",{})
		}
	simulateEndTurn():void
		{
		var self = this;
		this.gameService.stateManager.websocketConnector.send("/app/player/end-turn",{})
		}
	simulateBuy():void
		{
		var self = this;
		let cpid = this.gameService.stateManager.game.currentPlayer.id
		this.gameService.stateManager.websocketConnector.send("/app/player/buy/"+cpid,{})
		}


	}
