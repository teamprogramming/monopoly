import { Component, Input, OnInit } from '@angular/core';
import {Game} from '../../Classes/Game';
import {Field} from '../../Classes/Field';
@Component({
  selector: 'game-table',
  templateUrl: './game-table.component.html'
})
export class GameTableComponent implements OnInit
{
@Input() game:Game;
upperFields:Field[];
leftFields:Field[];
rightFields:Field[];
downFields:Field[];
specialFields:Field[];

ngOnInit():void
	{
	this.upperFields = this.game.fields.slice(1,10);
	this.leftFields = this.game.fields.slice(31,30+10).reverse();
	this.rightFields = this.game.fields.slice(11,10+10);
	this.downFields = this.game.fields.slice(21,20+10).reverse();
	this.specialFields = [this.game.fields[0],this.game.fields[10],this.game.fields[20],this.game.fields[30]]
	}

onMouseout():void
	{
	this.game.displayUtility.highlightedField=null;
	}
}
