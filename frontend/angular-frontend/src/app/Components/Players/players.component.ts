import { Component, Input } from '@angular/core';
import {Player} from '../../Classes/Player';
@Component({
  selector: 'players',
  templateUrl: './players.component.html'
})
export class PlayersComponent
{
@Input() players:Player[];
}
