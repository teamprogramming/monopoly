import { Component, Input } from '@angular/core';
import {Field} from '../../Classes/Field'
import {Game} from '../../Classes/Game'
@Component({
  selector: 'field',
  templateUrl: './field.component.html',
	styleUrls: ['./field.component.css']
})
export class FieldComponent
{
@Input() field:Field;
@Input() orientation:string;
//isOvered:boolean=false;

orientationDict=
	{
	"up":["flex-column","h-25"],
	"down":["flex-column-reverse","h-25"],
	"left":["flex-row","h-100 w-25"],
	"right":["flex-row-reverse","h-100 w-25"],
	"edge":["flex-column-reverse","h-25"],
	"edge_ur":["flex-column ","w-25 h-25"],
	"edge_ul":["flex-column align-items-end","w-25 h-25"],
	"edge_dr":["flex-column-reverse","w-25 h-25"],
	"edge_dl":["flex-column-reverse align-items-end","w-25 h-25"],
	};

onMouseover():void
	{
	//this.isOvered=true;
	this.field.game.displayUtility.highlightedField=this.field;
	}
/*
onMouseout():void
	{
	this.isOvered=false;
	this.field.game.displayUtility.highlightedField=null;
	}
*/
}
