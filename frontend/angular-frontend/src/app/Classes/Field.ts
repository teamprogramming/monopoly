import {Player} from './Player';
import {Game} from './Game';
import {FieldInfo} from './FieldInfo';

export interface Field
{
position:number;
owner:Player;
game:Game;
isBuyable:boolean;

isBuildable:boolean;
houseNum:number;
hotelNum:number;
//fieldInfo:FieldInfo;

name:string;
housePrice?:number;
hotelPrice?:number;
buyPrice?:number;
stayPrice:number;
color:string;//leave null for white
symbol:string;//leave null for none
description:string;
utilityStayPrice?:number;
}
