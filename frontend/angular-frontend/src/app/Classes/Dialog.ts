import {Game} from './Game';
import {WebsocketConnector} from './WebsocketConnector';
export class Dialog
{
dialogType:string; //"WAIT, TURN, ROLL"

//new and improved:
websocketConnector:WebsocketConnector;
displayedText:string="";
canPlayerRoll:boolean=false;
canPlayerBuyField:boolean=false;
canPlayerEndTurn:boolean=false;

canPlayerGetOutOfJail:boolean=false;
canPlayerBail:boolean=false;
fieldPrice:number=0;
housePrice:number=0;
hotelPrice:number=0;
saldo:number=0;
//statefull


statePlayerCanRoll=false;
statePlayerCanDoubleRoll=false;
stateTurnStartCash=0;
statePlayerStartedTurnInJail=false;
//websocketConnector:WebsocketConnector = new WebsocketConnector();

game:Game;
constructor(game:Game)
	{
	this.game = game;
	this.websocketConnector=game.websocketConnector;
	}

buyField():void
	{
	let pid = this.game.currentPlayer.id;
	let cid = this.game.currentPlayer.field.position;
	this.websocketConnector.send("/app/player/buy/"+pid,{})


	}

useGetOutOfJailCard():void
	{
	let pid = this.game.currentPlayer.id;
	let cid = this.game.currentPlayer.field.position;
	this.websocketConnector.send("/app/player/jail/card/"+pid,{})
	}

payBail():void
	{
	let pid = this.game.currentPlayer.id;
	let cid = this.game.currentPlayer.field.position;
	this.websocketConnector.send("/app/player/jail/bail/"+pid,{})
	}








rollDice():void
	{
	this.websocketConnector.send("/app/player/roll",{})

	this.game.firstTime = false;
	//this.game.currentUserPlayerTurnStartFlag=false;
	this.statePlayerCanRoll=false;
	this.statePlayerCanDoubleRoll=false;

	//this.updateDialog();
	}

endTurn():void
	{
	this.websocketConnector.send("/app/player/end-turn",{})
	//this.game.currentUserPlayerTurnStartFlag=false;

	}



restBuild(house:boolean,fieldId:number):void
	{
	let building = "house";
	if(!house){building="hotel"}
	var obs =  this.game.http.post("/build/"+building+"/"+fieldId,{fieldId:fieldId});
	obs.subscribe(
		data=>{console.log("POST data",data)},
		err=>{console.log("POST err",err)},
		()=>{console.log("POST complete")}
		)
	}

private getFieldsOfSameColor(field):any
	{
	return this.game.fields.filter((f)=>{return f.color == field.color})
	}






updateDialog():void
	{
	var self = this;
	//console.log("=========fields:",self.getFieldsOfSameColor(self.game.fields[1]))
	this.updateCosts();
	//console.log("GAME:",this.game.currentUserPlayerTurnStartFlag)
	this.viewClear();
	if(this.game.isUserCurrent)
		{
		this.viewTurn();
		}
	else
		{
		this.viewEndTurn();
		this.saldo = 0;
		}
	this.updateSaldo();
	}

updateSaldo():void
	{
	if(this.game.currentUserPlayerTurnStartFlag)
		{
		this.stateTurnStartCash=this.game.userPlayer.cash;
		}
	this.saldo = this.game.userPlayer.cash - this.stateTurnStartCash;
	}

updateCosts():void
	{
	this.fieldPrice = this.game.userPlayer.field.buyPrice;
	this.housePrice = this.game.userPlayer.field.housePrice;
	this.hotelPrice = this.game.userPlayer.field.hotelPrice;
	}

//-------------
private viewClear():void
	{
		this.displayedText=""
		this.canPlayerRoll=false;
		this.canPlayerBuyField=false;
		this.canPlayerEndTurn=false;

		this.canPlayerGetOutOfJail=false;
		this.canPlayerBail=false;
	}
private viewEndTurn():void
	{
	if(!this.game.userPlayer.isBankrupt)
		{
	this.displayedText="Your turn is over, please wait"
		}
	else
		{
	this.displayedText="You are bankrupt. The game goes on without you."
		}


	this.statePlayerCanRoll=false;
	this.statePlayerCanDoubleRoll=false;
	this.statePlayerStartedTurnInJail=false;
	}

private viewTurn():void
	{
	if(this.game.currentUserPlayerTurnStartFlag)
		{
		this.statePlayerCanRoll=true;
		if(this.game.userPlayer.inJail)
			{
			this.statePlayerStartedTurnInJail=true;
			}
		}

	if(this.game.currentPlayerDoubles)
		{
		this.statePlayerCanDoubleRoll=true;
		}

	if (this.game.userPlayer.inJail)
		{
		this.viewJail();
		}
	else if (this.statePlayerCanRoll)
		{
		this.viewRoll();
		}
	else
		{
		this.viewBuy();
		}

	}

viewJail():void
	{
	if (this.statePlayerStartedTurnInJail)
		{
		this.displayedText="Please wait for a release message, or try to double-roll!"
		if(this.game.userPlayer.hasGetOutOfJailCard)
			{this.canPlayerGetOutOfJail=true;}

		if(this.statePlayerCanRoll)
			{this.canPlayerRoll=true;}
		else
			{this.canPlayerEndTurn=true;}

		if(this.game.userPlayer.cash > 50)
			{this.canPlayerBail=true;}
		}
	else
		{
		this.displayedText=""
		this.canPlayerEndTurn=true;
		}

	}

viewRoll():void
	{
	this.displayedText="It's your turn! Roll the dice"
	this.canPlayerRoll=true;
	}



viewBuy():void
	{

	if (this.game.currentPlayer.field.owner==null && this.game.currentPlayer.field.isBuyable && this.game.currentPlayer.field.buyPrice < this.game.currentPlayer.cash )
		{
		this.viewBuyField()
		}
	else if(this.game.currentPlayer.field.owner && this.game.currentPlayer.field.owner.id == this.game.currentPlayer.id )
		{
		this.viewBuyBuildings()
		}
	if(this.statePlayerCanDoubleRoll)
		{
		this.displayedText+=" Double Roll!! "
		this.canPlayerRoll=true;
		this.canPlayerEndTurn=false;
		}
	else
		{
		this.displayedText=""
		this.canPlayerEndTurn=true;
		}
	}

viewBuyField():void
	{
	this.displayedText="This is an empty parcel."
	this.canPlayerEndTurn=true;
	this.canPlayerBuyField=true;
	}

viewBuyBuildings():void
	{
	this.displayedText="This is your parcel."
	this.canPlayerEndTurn=true;
	}

}
