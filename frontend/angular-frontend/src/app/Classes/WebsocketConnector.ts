import * as Stomp from "stompjs";
import * as SockJS from "sockjs-client";

export class WebsocketConnector
{
stompClient:any=null;
connect(url:String,onConnected:()=>any):void
	{
	let socket = new SockJS(url);
	this.stompClient = Stomp.over(socket);
	this.stompClient.connect({}, (frame)=>
		{
		console.log('Connected: ' + frame);
		onConnected();
		/*
		this.stompClient.subscribe(subscriptionAddr, (msg)=>
			{
			console.log("received:",msg)
      });
		*/
    });
	}
subscribe(subscriptionAddr,onMessage:(msg:any)=>any):void
	{
	this.stompClient.subscribe(subscriptionAddr, (msg)=>
		{
		if(msg.body)
			{
			console.log(subscriptionAddr+": received:",JSON.parse(msg.body))
			onMessage(JSON.parse(msg.body));
			}
		else
			{console.log(subscriptionAddr+": received:",msg)}
    });
	}
send(destination:String,message:any):void
	{
  this.stompClient.send(destination, {}, message);
	}
disconnect()
	{
	if (this.stompClient !== null)
		{
		this.stompClient.disconnect();
		}
	}

}
