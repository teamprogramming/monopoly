import {Game} from './Game';
import {Field} from './Field';
export interface Player
{
name:string;
id:number;
cash:number;
field:Field;
game:Game;
inJail:boolean;
hasGetOutOfJailCard:boolean;
isBankrupt:boolean;
}
