
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Game} from './Game';
import {WebsocketConnector} from './WebsocketConnector';

export class StateManager
{
state:string="NO_GAME"; // WAITING, GAME, GAME_OVER -- no game when url doesnt give game, others self descriptive
waitingPlayers:number=0;
targetPlayers:number=0;
game:Game;
websocketConnector:WebsocketConnector = new WebsocketConnector();
playerName:string;
userPlayerId:number;

firstTime:boolean =true;
//functionality: has a function: on json arrived, which validates json, and sets variables in game/creates it.
//on those variables all depends.



constructor(public http:HttpClient)
	{

	this.state = "WAITING";
	var self = this;
	this.game = new Game(4,http,this.websocketConnector,self);
	this.configureWebSockets();
	}


unmarshallGameState(stateObject:any):void
	{
	var self = this;
	console.log("unmarshalling game state :",stateObject);
	if(!stateObject.gameStarted)
		{
		this.state = "WAITING";
		this.waitingPlayers=stateObject.playersConnected;
		this.targetPlayers=4;
		}
	else
		{
		if(this.state !="GAME")
			{
			this.game.currentPlayer=this.game.players[0];

			}
		this.state = "GAME";

		}
	//this.checkDialog();
	}


unmarshallPlayers(players:any,nextPlayerId=undefined):void
	{

	let dataObject =
		{
		fields:[],
		players:[],
		gameData:{}
		}


	for(let player of players)
		{
		//this.unmarshallPlayer(p);

		let newPlayer =
			{
			name:player.name,
			cash:player.accountBalance,
			id:player.playerId,
			fieldId:player.playerPosition,
			messageToDisplay:player.messageToDisplay,
			inJail:player.inJail,
			hasGetOutOfJailCard:player.hasGetOutOfJailCard,
			isBankrupt:player.bankrupt
			}

		for(let card of player.cards)
			{
			dataObject.fields.push({position:card.cardId,ownerId:player.playerId,hotelNum:card.numberOfHotels,houseNum:card.numberOfHouses});
			}

		let userPlayerId:number = undefined;
		if(player.name == this.playerName)
			{
			userPlayerId = player.playerId;
			}

		let doublesInRow:boolean = false;
		if(player.doublesInRow>0)
			{
			doublesInRow=true;
			}

		let newGameData =
			{
			currentPlayerId:nextPlayerId,
			currentPlayerDoubles:doublesInRow,
			userPlayerId:userPlayerId
			}
		dataObject.players.push(newPlayer);
		dataObject.gameData = newGameData;
		}
	this.game.updateGame(dataObject);
	}

unmarshallNextPlayer(player):void
	{
	this.unmarshallPlayers([player],player.playerId);
	}




configureWebSockets():void
	{

	const subAddrs =
		{
		"/topic/player/moves":(p)=>{this.unmarshallPlayers([p]);},
		"/topic/bought-properties":(p)=>{this.unmarshallPlayers([p]);},
		"/topic/players":(ps)=>{this.unmarshallPlayers(ps);},
		"/topic/game-start-state":(gs)=>{this.unmarshallGameState(gs);},
		"/topic/player/next":(p)=>{this.unmarshallNextPlayer(p);},
		}

	const onconnect = ()=>
		{
		for(let k in subAddrs)
			{
			this.websocketConnector.subscribe(k,subAddrs[k])
			}
		}

	this.websocketConnector.connect("/game",onconnect)
	}




addPlayer(name:string):void
	{
	this.playerName = name;
	var obs =  this.http.post("/player/add/"+name,{name:name});
	obs.subscribe(
		data=>{console.log("POST data",data)},
		err=>{console.log("POST err",err)},
		()=>{console.log("POST complete")}
		)
	}

endGame():void
	{
	this.state = "GAME_OVER"
	}

}
