import {Player} from './Player';
import {Game} from './Game';
import {Field} from './Field';

export interface DisplayUtility
{
highlightedPlayer:Player;
highlightedField:Field;
}
