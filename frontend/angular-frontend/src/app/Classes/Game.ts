import {Player} from './Player';
import {Field} from './Field';
import {Dialog} from './Dialog';
import {DisplayUtility} from './DisplayUtility';
import {FieldInfo} from './FieldInfo';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {WebsocketConnector} from './WebsocketConnector';
import {StateManager} from './StateManager'

export class Game
	{
	players:Player[]=[];
	fields:Field[]=[];
	dialog:Dialog;
	userPlayer:Player;
	websocketConnector:WebsocketConnector;
	//fieldInfos : { [key:string]:FieldInfo; } = {};

	displayUtility:DisplayUtility;

	currentPlayer:Player;


	//isPlayerThrowing:boolean=true;
	currentPlayerDoubles:boolean=false;
	currentUserPlayerTurnStartFlag:boolean=true;
	currentUserPlayerTurnStartFlagHelper:boolean=true;
	isUserCurrent:boolean=false;
	userPlayerStepNum:number=0;
	userPlayerLastPos:number=0;
	http:HttpClient;

	firstTime:boolean=true;

	messageCallback:any=(messages)=>{console.log(">>>>>>messages:",messages)}

	stateManager:StateManager;
	constructor(numOfPlayers:number,http:HttpClient,websocketConnector:WebsocketConnector,stateManager:StateManager)
		{
		var self = this;
		this.websocketConnector=websocketConnector;
		this.stateManager = stateManager;
		this.http = http;
		for(var i=0; i<40; i++)
			{
			this.fields.push(
				{
				position:i,
				owner:null,
				game:self,
				isBuyable:true,
				isBuildable:true,
				houseNum:0,
				hotelNum:0,
				name:"none",
				housePrice:5,
				hotelPrice:6,
				buyPrice:10,
				stayPrice:2,
				color:"red",
				symbol:null,
				description:"lorem ipsum"
				});
			}

		for(var i=0; i<numOfPlayers; i++)
			{
			this.players.push({name:"joe",id:i,cash:0,game:self,field:self.fields[0],inJail:false,hasGetOutOfJailCard:false,isBankrupt:false});
			}
		this.displayUtility = {highlightedPlayer:null,highlightedField:null};

		this.dialog = new Dialog(self);
		this.dialog.dialogType="WAIT"
		//this.testFetching("/card/board");
		this.updateCards();
		}


	/*
	dataObj:
		{
		fields:
			[{
				position:i,
				ownerId:null,
				isBuyable:true,
				isBuildable:true,
				houseNum:0,
				hotelNum:0,
				name:"none",
				housePrice:5,
				hotelPrice:6,
				buyPrice:10,
				stayPrice:2,
				color:"red",
				symbol:null,
				description:"lorem ipsum"
			}],
		players:
			[{
			name:"joe",
			id:i,
			cash:0,
			fieldId:integer
			}],
		gameData:
			[{
			currentPlayerId:integer
			currentPlayerDoubles:boolean
			userPlayerId:integer
			}]
		}
	*/


	getPlayerById(id:number):Player
		{
		for(let player of this.players)
			{
			if (player.id == id)
				{
				return player;
				}
			}
		return null;
		}

	updateGame(dataObject):void
		{
		console.log("updating game:",dataObject);

		for (let field of dataObject.fields)
			{
				this.fields[field.position].owner = this.getPlayerById(3)
			for (let key of Object.keys(field))
				{

				if (key=="ownerId")
					{
					this.fields[field.position].owner = this.getPlayerById(field["ownerId"])

					}
				else
					{
					let tf = this.fields[field.position]
					tf[key] = field[key] || tf[key]
					}

				}
			}

		//TEMP START
/*
		for (let f of this.fields)
			{
			f.houseNum=4;
			//f.hotelNum=1;
	//		if ( [1,6,13,21,26,31,37].indexOf(f.position) == -1 )
	//			{f.owner = this.getPlayerById(3);}
			if ( [1,6,13,21,26,31,37].indexOf(f.position) != -1 )
				{f.houseNum=3;}
			f.owner = this.getPlayerById(3);

			}
*/
		//TEMP END


		for(let player of dataObject.players)
			{
			let p = this.getPlayerById(player.id);
			p.name = player.name;
			p.cash = player.cash;
			p.field = this.fields[player.fieldId]
			p.inJail = player.inJail;
			p.hasGetOutOfJailCard = player.hasGetOutOfJailCard;
			p.isBankrupt = player.isBankrupt;
			if(this.userPlayer && player.id==this.userPlayer.id && player.messageToDisplay && player.messageToDisplay.length >0  )
				{
				//console.log("FFS MESSAGES:",player.messageToDisplay)
				this.messageCallback(player.messageToDisplay);
				}
			}

		this.currentPlayer = this.getPlayerById(dataObject.gameData.currentPlayerId) || this.currentPlayer
		this.userPlayer = this.getPlayerById(dataObject.gameData.userPlayerId) || this.userPlayer
		//this.isPlayerThrowing = dataObject.gameData.currentPlayerThrowing;
		this.currentPlayerDoubles=dataObject.gameData.currentPlayerDoubles;
		//hamfisted but whatevs

//TEMP
/*
	for (let p of this.players)
		{
		if (p.id == 3)
			{
			p.isBankrupt = true;
			}
		}
*/
//TEMP END


		this.updateCards();
		this.cleanBankrupts();
		}

	cleanBankrupts():void
		{
		for(let p of this.players)
			{
			if(p.isBankrupt)
				{
				for(let f of this.fields)
					{
					if(f.owner && f.owner.id == p.id)
						{
						f.owner = null;
						f.houseNum = 0;
						f.hotelNum = 0;
						}
					}
				}
			}
		}




	//left just in case
	testFetching(url:string):void
		{
		var obs =  this.http.get(url);
		obs.subscribe(
			data=>{console.log("data",data)},
			err=>{console.log("err",err)},
			()=>{console.log("complete")}
			)
		}

	updateCards():void
		{
		var self = this;
		var url = "/card/board"
		var obs =  this.http.get(url);
		obs.subscribe(
			data=>{/*console.log("data",data);*/ self.onCardData(data);},
			err=>{console.log("err",err)},
			()=>{/*console.log("complete")*/}
			)
		}

	private onCardData(dataObj:any):void
		{
		console.log("card data",dataObj);
		var colorDict =
			{
			BROWN:"Brown",
			LIGHT_BLUE:"LightBlue",
			PINK:"Pink",
			ORANGE:"Orange",
			RED:"Red",
			YELLOW:"Yellow",
			GREEN:"Green",
			DARK_BLUE:"DarkBlue"
			}

		var symbolDict =
			{
			"CITY_CARD":null,
			"CHANCE":"icon_main_exclamation_mark",
			"COMMUNITY_CHEST":"icon_main_community_chest",
			"RAILROAD":"icon_main_circle",
			"UTILITY":"icon_main_triangle",
			"TAX":"icon_main_dollar",
			"GO":"icon_corner_star",
			"JAIL":"icon_corner_lock",
			"PARKING":"icon_corner_free_stay",
			"GO_TO_JAIL":"icon_corner_triangle"
			}

		var valida = function(price:any):boolean
			{
			if(price)
				return true
			else
				return false
			}

		var staycost =function(objs:any):number //will need to be updated as cost will be served, rather than calculated
			{
			if (objs.penalizationCosts)
				{
				return objs.penalizationCosts[objs.numberOfHouses+objs.numberOfHotels]
				}
			else
				{return 0}
			}

		for(let o of dataObj.fields)
			{


			var f = this.fields[o.cardId];
			f.isBuyable = valida(o.price);  //UNFINISHED
			f.isBuildable = valida(o.buildCost); //UNFINISHED
			f.houseNum= f.houseNum || 0;
			f.hotelNum= f.hotelNum || 0;
			f.name= o.name;
			f.housePrice= o.buildCost || undefined;
			f.hotelPrice= o.buildCost || undefined;
			f.buyPrice= o.price || undefined;
			f.stayPrice= staycost(o);
			f.color= colorDict[o.color];
			f.symbol= symbolDict[o.type];
			f.description= o.type;
			this.updateCardPenalizationCost(o.cardId);
			}
		this.onGameUpdated();
		}

	updateCardPenalizationCost(id):void
		{
		var self = this;
		var url = "/card/penalization/"+id
		var obs =  this.http.get(url);
		var id = id;
		obs.subscribe(
			data=>{ self.onPenalizationCost(id,data);},
			err=>{console.log("err",err)},
			()=>{/*console.log("complete")*/}
			)
		}


	private onPenalizationCost(id,dataObj):void
		{

		if (dataObj!=0)
		{console.log("PENALIZATION: id:",id," COST:",dataObj)}

		this.fields[id].stayPrice = dataObj
		if(this.fields[id].description=="UTILITY")
			{
			this.fields[id].utilityStayPrice = dataObj*this.userPlayerStepNum;
			}
		}

	initMessageCallback(callback:any)
		{
		this.messageCallback = callback;
		}


	onGameUpdated():void
		{
		this.updateStepper();
		this.updateTurnStart();
		this.checkGameOver();
		}

	updateStepper():void
		{
		if(this.userPlayer.field.position != this.userPlayerLastPos)
			{
			if(this.userPlayer.field.position < this.userPlayerLastPos)
				{
				this.userPlayerStepNum += 40 - this.userPlayer.field.position + this.userPlayerLastPos
				}
			else
				{
				this.userPlayerStepNum += this.userPlayer.field.position - this.userPlayerLastPos
				}
			this.userPlayerLastPos = this.userPlayer.field.position;
			}
		}

	checkGameOver():void
		{
		let outarr = this.players.filter((x)=>{return x.isBankrupt})
		if (outarr.length == 3)
			{
			this.stateManager.endGame();
			}
		}

	updateTurnStart():void
		{
		if (this.currentPlayer.id==this.userPlayer.id)
			{
			this.isUserCurrent=true;
			}
		else
			{
			this.isUserCurrent = false;
			}

		this.currentUserPlayerTurnStartFlag=false;
		if (this.isUserCurrent && this.currentUserPlayerTurnStartFlagHelper)
			{
			this.currentUserPlayerTurnStartFlag=true;
			}

		if (this.isUserCurrent && this.currentUserPlayerTurnStartFlagHelper)
			{
			this.currentUserPlayerTurnStartFlagHelper =false
			}
		else if (!this.isUserCurrent)
			{
			this.currentUserPlayerTurnStartFlagHelper =true
			}


		this.dialog.updateDialog();
		}

	}
