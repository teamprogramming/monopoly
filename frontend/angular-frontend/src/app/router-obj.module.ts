import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {StartComponent} from './Components/Start/start.component';
import {GameComponent} from './Components/Game/game.component';

const routes: Routes = [
  { path: '', component: StartComponent },
  { path: 'game', component: GameComponent }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class RouterObjModule {}
